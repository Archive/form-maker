/* -*- Mode: C -*-
 * Copyright 1998, 1999 Gregory McLean
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */
#ifndef __EDITOR_H__
#define __EDITOR_H__

#include <config.h>
#include <sys/types.h>
#include <gtk/gtk.h>


GtkWidget          *form_panel          (const char       *which);
GtkWidget          *field_form          (const char       *title);

#endif

/* EOF */

