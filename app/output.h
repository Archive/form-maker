/* -*- Mode: C -*-
 * Copyright 1999 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Output routines.
 */
#ifndef __OUTPUT_H__
#define __OUTPUT_H__
#include "worksheet.h"
#include "dialog.h"
#include "gtkprintdialog.h"

/****************************************************************************
 * Forward references
 ***************************************************************************/
void            writeProlog                  (forms_def     *form,
					      FILE          *out_fd);
void            writeWordWrap                (FILE          *out_fd);
void            emitField                    (wks_data      *datum,
					      FILE          *out_fd);
void            makeworksheet                (GSList        *list,
				              forms_def     *form,
				              GtkPrinterDef *printer,
				              gboolean      gui);
gint            open_template                (gchar         *file,
					      gboolean      gui);
void            save_form_config             (const gchar   *pathname,
					      forms_def     *form);
void            write_form_config            (const char    *pathname,
					      forms_def     *form);
FILE            *open_output                 (GtkPrinterDef *printer,
					      gboolean      gui);
void            non_interactive_output       (forms_def     *form,
					      char          *datum,
					      char          *end);

#endif

/* EOF */



