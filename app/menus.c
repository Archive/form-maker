/* -*- Mode: C -*-
 * $Id$
 * Form Maker  -- A premade form filler outer.
 *
 * Copyright 1999 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
extern GtkWidget   *app_window;
/****************************************************************************
 * Support for the "dialogs" menu.
 ***************************************************************************/
typedef struct _form_dialog
{
  GtkWidget   *dialog;           /* Widgets handle for an open dialog */
  gpointer    dialog_data;       /* Pointer to data structure being edited. */
  gchar       *dialog_type;      /* Name assigned to the dialog */
  gchar       *dialog_title;     /* The title to display */
  gchar       *dialog_menu_path; /* Saved menu path for later delete */
} form_dialog;

static GList   *dialogs = NULL;
static void    dialog_activate_cb           (GtkWidget     *widget,
					     gpointer      data);
static void    dialog_destroy_cb            (GtkWidget     *widget,
					     gpointer      data);
/****************************************************************************
 * menu callbacks.
 ***************************************************************************/
static void    file_exit_cb                 (GtkWidget     *widget,
					     gpointer      data);

static void    help_about_cb                (GtkWidget     *widget,
					     gpointer      data);
static void    about_close                  (GtkWidget     *widget,
					     gpointer      data);

static void    form_file_open_cb            (GtkWidget     *widget,
					     gpointer      data);
static void    form_file_save_cb            (GtkWidget     *widget,
					     gpointer      data);
static void    form_file_saveas_cb          (GtkWidget     *widget,
					     gpointer      data);
static void    form_file_print_cb           (GtkWidget     *widget,
					     gpointer      data);
static void    form_file_close_cb           (GtkWidget     *widget,
					     gpointer      data);
static void    form_file_exit_cb            (GtkWidget     *widget,
					     gpointer      data);

static void    form_edit_clear_cb           (GtkWidget     *widget,
					     gpointer      data);
static void    edit_properties_cb           (GtkWidget     *widget,
					     gpointer      data);
/****************************************************************************
 * The file menu for the main app window.
 ***************************************************************************/
static GnomeUIInfo file_menu[] = {
  { GNOME_APP_UI_ITEM, N_("Exit"), NULL, file_exit_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_EXIT, 
    'X', GDK_CONTROL_MASK, NULL },
  { GNOME_APP_UI_ENDOFINFO }
};
static GnomeUIInfo form_file_menu[] = {
  { GNOME_APP_UI_ITEM, N_("Open"), NULL, form_file_open_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_OPEN,
    0, 0, NULL },
  { GNOME_APP_UI_ITEM, N_("Save"), NULL, form_file_save_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE,
    0, 0, NULL },
  { GNOME_APP_UI_ITEM, N_("Save As"), NULL, form_file_saveas_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE_AS,
    0, 0, NULL },
  GNOMEUIINFO_SEPARATOR,
  { GNOME_APP_UI_ITEM, N_("Print"), NULL, form_file_print_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PRINT,
    0, 0, NULL },
  GNOMEUIINFO_SEPARATOR,
  { GNOME_APP_UI_ITEM, N_("Close"), NULL, form_file_close_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CLOSE,
    0, 0, NULL },
  { GNOME_APP_UI_ITEM, N_("Exit"), NULL, form_file_exit_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_EXIT,
    'X', GDK_CONTROL_MASK },
  { GNOME_APP_UI_ENDOFINFO }
};
enum {
  APPMENU_FILE_EXIT
};
/****************************************************************************
 * The main panel edit menu
 ***************************************************************************/
static GnomeUIInfo edit_menu[] = {
  { GNOME_APP_UI_ITEM, N_("Properties"), NULL, edit_properties_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BLANK, 0, 0, NULL },
  { GNOME_APP_UI_ENDOFINFO }
};

/****************************************************************************
 * The form panel edit menu 
 ***************************************************************************/
static GnomeUIInfo form_edit_menu[] = {
  { GNOME_APP_UI_ITEM, N_("Clear Form"), NULL, form_edit_clear_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BLANK,
    0, 0, NULL },
  { GNOME_APP_UI_ENDOFINFO }
};
/****************************************************************************
 * The dialog menu
 ***************************************************************************/
static GnomeUIInfo dialog_menu[] = {
  { GNOME_APP_UI_ENDOFINFO }
};
/****************************************************************************
 * The 'Help' menu
 ***************************************************************************/
static GnomeUIInfo help_menu[] = {
  { GNOME_APP_UI_ITEM, N_("About Form Maker"),
    N_("Information about this program."), help_about_cb, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_ABOUT, 'A', GDK_CONTROL_MASK,
    NULL },
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_HELP ("form_maker"),
  { GNOME_APP_UI_ENDOFINFO }
};
enum {
  APPMENU_HELP_ABOUT
};
/****************************************************************************
 * The main application menu.
 ***************************************************************************/
static GnomeUIInfo form_maker_menu[] = {
  { GNOME_APP_UI_SUBTREE, N_("_File"), NULL, file_menu, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL },
  { GNOME_APP_UI_SUBTREE, N_("_Edit"), NULL, edit_menu, NULL, NULL, 
    GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL },
  { GNOME_APP_UI_SUBTREE, N_("_Dialogs"), NULL, dialog_menu, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL },
  { GNOME_APP_UI_SUBTREE, N_("_Help"), NULL, help_menu, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL },
  { GNOME_APP_UI_ENDOFINFO }
};
enum {
  APPMENU_FILE,
  APPMENU_EDIT,
  APPMENU_DIALOG,
  APPMENU_HELP
};
/****************************************************************************
 * The data entry window menu.
 ***************************************************************************/
static GnomeUIInfo form_menu[] = {
  { GNOME_APP_UI_SUBTREE, N_("_File"), NULL, form_file_menu, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL },
  { GNOME_APP_UI_SUBTREE, N_("_Edit"), NULL, form_edit_menu, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL },
  { GNOME_APP_UI_SUBTREE, N_("_Help"), NULL, help_menu, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL },
  { GNOME_APP_UI_ENDOFINFO }
};
/****************************************************************************
 * Create menus, greying out menus that are not active yet.
 ***************************************************************************/
void
menu_create (GtkWidget *main_window)
{
  gnome_app_create_menus_with_data (GNOME_APP (main_window), form_maker_menu,
				    main_window);
  gtk_widget_set_sensitive (form_maker_menu[APPMENU_DIALOG].widget, FALSE);
}

void
form_menu_create (GtkWidget *dialog)
{
  gnome_app_create_menus_with_data (GNOME_APP (dialog), form_menu,
				    dialog);
}
/****************************************************************************
 * Menu callbacks.
 ***************************************************************************/
static void
file_exit_cb (GtkWidget *widget, gpointer data)
{
  gtk_main_quit (); /* This is sloppy! */
}

static GtkWidget *about = NULL;
static void
help_about_cb (GtkWidget *widget, gpointer data)
{
  const gchar  *authors[] = {
    "Gregory McLean",
    NULL
  };
  if (!about)
    {
      about = gnome_about_new ( "Form Maker", VERSION,
				"Copyright 1998, 1999 Gregory McLean",
				(const gchar **) authors,
				_("Form Maker is a form generation"
				  " and completion application to stream "
				  "line your paperwork needs."), 
				NULL);
      gtk_signal_connect (GTK_OBJECT (about), "destroy", 
			  (GtkSignalFunc) about_close, NULL);
      gtk_widget_show (about);
    }
}
static void
about_close (GtkWidget *widget, gpointer data)
{
  about = NULL;
}

static void
form_file_open_cb (GtkWidget *widget, gpointer data)
{
}

static void
form_file_save_cb (GtkWidget *widget, gpointer data)
{
}

static void
form_file_saveas_cb (GtkWidget *widget, gpointer data)
{
}

static void
form_file_print_cb (GtkWidget *widget, gpointer data)
{
  gtk_widget_show_all (GTK_WIDGET (create_print_window (data)));
}

static void
form_file_close_cb (GtkWidget *widget, gpointer data)
{
  gtk_widget_destroy (GTK_WIDGET (data));
}

static void
form_file_exit_cb (GtkWidget *widget, gpointer data)
{
  gtk_main_quit (); /* Very sloppy */
}

static void 
form_edit_clear_cb (GtkWidget *widget, gpointer data)
{
  clear_entry_window (GTK_WIDGET (data));
}

static void
edit_properties_cb (GtkWidget *widget, gpointer data)
{
}

/****************************************************************************
 * Dialog management
 *
 * dialog_destroy_cb is invoked when a dialog object is destroyed.  It removes
 * the dialog from the dialog list, and removes  the corresponding menu item.
 * When all dialogs have been removed it disables the dialog menu.
 ***************************************************************************/
static void
dialog_destroy_cb (GtkWidget * dialog, gpointer data)
{
  form_dialog * dialog_struct;
  g_return_if_fail (data != NULL);
  dialog_struct = (form_dialog *) data;
  gnome_app_remove_menus (GNOME_APP (app_window),
                          dialog_struct->dialog_menu_path, 1);

  dialogs = g_list_remove (dialogs, dialog_struct);
#if 0
  if (dialog_struct->dialog_title)
    g_free (dialog_struct->dialog_title);
#endif
  if (dialog_struct->dialog_menu_path)
    g_free (dialog_struct->dialog_menu_path);
  if (g_list_length (dialogs) == 0)
    gtk_widget_set_sensitive (form_maker_menu[APPMENU_DIALOG].widget, FALSE);
}
/****************************************************************************
 *  dialog_activate_cb is invoked when the user selects a dialog from the
 *  dialog menu
 ***************************************************************************/
static void
dialog_activate_cb (GtkWidget * widget, gpointer data)
{
  GtkWidget * dialog;
  g_return_if_fail (data != NULL);
  dialog = GTK_WIDGET (data);
  gdk_window_show (dialog->window);     /* deiconify */
  gdk_window_raise (dialog->window);    /* pull it to the top in the stack */
}
void     
form_dialog_add (GtkWidget * dialog,
		   gchar     * dialog_type,
	           gpointer    dialog_data,
	           gchar     * dialog_title)
{
  form_dialog * dialog_struct;
  gchar		    * path;
  GnomeUIInfo       * menu;
  gchar		    * title;
  dialog_struct               = g_new (form_dialog, 1);
  if (! (dialog_struct))
    return;

  dialog_struct->dialog       = dialog;
  dialog_struct->dialog_data  = dialog_data;
  dialog_struct->dialog_type  = dialog_type;
  dialog_struct->dialog_title = dialog_title;

  gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
                      (GtkSignalFunc) dialog_destroy_cb, dialog_struct);

  dialogs = g_list_append (dialogs, dialog_struct);
  if (g_list_length (dialogs) >= 1)
    gtk_widget_set_sensitive (form_maker_menu[APPMENU_DIALOG].widget, TRUE);
  title = g_strdup_printf ("FormMaker -- %s", gettext(dialog_title));
  gtk_window_set_title (GTK_WINDOW (dialog), title);
  g_free (title);
  /*
  gnome_dialog_set_parent (GNOME_DIALOG (dialog), 
			   GTK_WINDOW (app_window));
  */
  path                  = g_strdup_printf ("%s/", gettext("_Dialogs"));
  menu 			= g_new0 (GnomeUIInfo, 2);
  if (!menu)
    {
      g_warning ("form_dialog_add: memory allocation failed for the "
		 "menu struct.");
      return;
    }
  menu->label           = g_strdup (dialog_title);
  menu->type            = GNOME_APP_UI_ITEM;
  menu->hint            = NULL;
  menu->moreinfo        = dialog_activate_cb;  /* Activation callback */
  menu->user_data       = dialog;              /* Widget for callback */
  menu->unused_data     = NULL;
  menu->pixmap_type     = 0;
  menu->pixmap_info     = NULL;
  menu->accelerator_key = 0;
  (menu + 1)->type        = GNOME_APP_UI_ENDOFINFO;
  gnome_app_insert_menus (GNOME_APP (app_window), path, menu);

  dialog_struct->dialog_menu_path = 
    g_strdup_printf ("%s/%s", gettext("_Dialogs"), gettext(dialog_title));

  gtk_widget_show (dialog);
}
/* EOF */
