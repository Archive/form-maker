/* -*- Mode: C -*-
 * $Id$
 * Copyright 1999, Gregory McLean 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * xml-io.h: Interfaces to save/read form maker form definitions and form
 *           data using XML encoding. This is based on the one found in
 *           gnumeric written by Daniel Veillard <Daniel.Veillard@w3.org>
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <stdio.h>
#include <gnome.h>

#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>

#include "worksheet.h"
#include "xml-io.h"

#include "debug.h"

/****************************************************************************
 * Helper functions
 *
 * xmlGetValue ()
 *
 * Get a value for a node either carried as an attribute or as the content of 
 * a child.
 ***************************************************************************/
static gchar *
xmlGetValue (xmlNodePtr node, const char *name)
{
  gchar      *ret;
  xmlNodePtr child;

  ret = (gchar *) xmlGetProp (node, name);
  if (ret != NULL)
    return ret;
  child = node->childs;
  
  while (child != NULL) 
    {
      if (!strcasecmp (child->name, name)) 
	{
	  /*
	   * !!! Inefficient, but ...
	   */
	  ret = xmlNodeGetContent(child);
	  if (ret != NULL)
	      return (ret);
	}
      child = child->next;
    }
  
  return NULL;
}

/****************************************************************************
 * xmlGetIntValue ()
 *
 * Get an integer value for a node either carried as an attribute or as the
 * content of a child. (Stolen from gnumeric)
 ***************************************************************************/
static gint
xmlGetIntValue (xmlNodePtr node, const gchar *name, gint *val)
{
  gchar *ret;
  gint i;
  gint res;
  
  ret = xmlGetValue (node, name);
  if (!ret) 
    return(0);
  res = sscanf (ret, "%d", &i);
  free(ret);
  
  if (res == 1) 
    {
      *val = i;
      return 1;
    }
  return 0;
}
/****************************************************************************
 * xmlSetIntValue ()
 *
 * Set an interger value for a node either carried as an attribute or as the
 * content of the child.
 ***************************************************************************/
static void
xmlSetIntValue (xmlNodePtr node, const gchar *name, gint val)
{
  const gchar *ret;
  xmlNodePtr child;
  gchar str[101];
  
  snprintf (str, 100, "%d", val);
  ret = xmlGetProp (node, name);
  if (ret != NULL)
    {
      xmlSetProp (node, name, str);
      return;
    }
  child = node->childs;
  while (child)
    {
      if (!strcmp (child->name, name))
	{
	  xmlNodeSetContent (child, str);
	  return;
	}
      
      child = child->next;
    }
  xmlSetProp (node, name, str);
}

/****************************************************************************
 * WriteXmlForm()
 *
 * Saves the form definition.
 ***************************************************************************/
gboolean
WriteXmlForm (forms_def *form, const gchar *filename)
{
  xmlDocPtr    doc;
  xmlNsPtr     form_ns;
  xmlNodePtr   tree;
  GSList       *gl;
  field_def    *field;
  gboolean     ret;

  doc = xmlNewDoc ("1.0");
  if (!doc)
    return -1;
  form_ns = xmlNewGlobalNs (doc, "http://www.gnome.org/", "fmaker");
  doc->root = xmlNewDocNode (doc, form_ns, "FormDefinition", NULL);
  xmlSetProp (doc->root, "Version", FORMMAKER_VERSION);
  xmlNewChild (doc->root, form_ns, "Group", form->group);
  xmlNewChild (doc->root, form_ns, "Name", form->name);
  xmlNewChild (doc->root, form_ns, "Template", form->path);
  xmlNewChild (doc->root, form_ns, "xmlEntry", form->xml);
  gl = form->field_list;
  while (gl)
    {
      field = (field_def *)gl->data;
      tree = xmlNewChild (doc->root, form_ns, "Field", NULL);
      xmlSetIntValue (tree, "Size", field->field_width);
      xmlSetIntValue (tree, "Type", field->field_type);
      xmlNewChild (tree, form_ns, "Name", field->field_name);
      xmlNewChild (tree, form_ns, "Macro", field->field_macro);
      xmlNewChild (tree, form_ns, "Default", field->field_default); 
      xmlNewChild (tree, form_ns, "ToolTip", field->field_tip);
      xmlNewChild (tree, form_ns, "Comment", field->field_comment);
      gl = gl->next;
    }
  ret = xmlSaveFile (filename, doc);
  xmlFreeDoc (doc);

  return (ret < 0) ? -1 : 0;
}
/****************************************************************************
 * ReadXmlForm ()
 *
 * Loads a form definition from the xml file.
 ***************************************************************************/
forms_def *
ReadXmlForm (const gchar *filename)
{
  xmlDocPtr    doc;
  xmlNodePtr   cur, fld;
  xmlNsPtr     form_ns;
  forms_def    *form;
  field_def    *field;
  gint         val;

  d_print (DEBUG_DUMP, "ReadXmlForm: parse %s\n", filename);
  doc = xmlParseFile (filename);
  if (doc == NULL)
    {
      g_warning ("ReadXmlForm: Unable to read form definition file %s",
		 filename);
      return NULL;
    }
  cur = doc->root;
  if (!cur)
    {
      g_warning ("ReadXmlForm: empty form definition.");
      xmlFreeDoc (doc);
      return NULL;
    }
  form_ns = xmlSearchNsByHref (doc, doc->root, "http://www.gnome.org/");
  if (doc->root->name == NULL || 
      strcasecmp (doc->root->name, "FormDefinition") ||
      form_ns == NULL)
    {
      g_warning ("ReadXmlForm: unrecognized file format.");
      xmlFreeDoc (doc);
      return NULL;
    }
  /*
   * Now we have a form...
   * Lets allocate some space and build up the definition.
   */
  form = (forms_def *)g_malloc0 (sizeof (forms_def));
  if (!form)
    {
      g_warning ("ReadXmlForm: failed to allocate memory for the form "
		 "definition.");
      xmlFreeDoc (doc);
      return NULL;
    }
  cur = cur->childs;
  while (cur)
    {
      if (!strcasecmp (cur->name, "Name"))
	form->name = xmlNodeListGetString (doc, cur->childs, 1);
      if (!strcasecmp (cur->name, "Group"))
	form->group = xmlNodeListGetString (doc, cur->childs, 1);
      if (!strcasecmp (cur->name, "Template"))
	form->path = xmlNodeListGetString (doc, cur->childs, 1);
      if (!strcasecmp (cur->name, "xmlEntry"))
	form->xml = xmlNodeListGetString (doc, cur->childs, 1);
      if (!strcasecmp (cur->name, "Field") )
	{
	  field = (field_def *)g_malloc0 (sizeof (field_def));
	  if (xmlGetIntValue (cur, "Size", &val))
	    field->field_width = val;
	  if (xmlGetIntValue (cur, "Type", &val))
	    field->field_type = val;
	  /* Do some type specific stuff here */

	  fld = cur->childs;
	  while (fld)
	    {
	      if (!strcasecmp (fld->name, "Name"))
		field->field_name       = 
		  xmlNodeListGetString (doc, fld->childs, 1);
	      if (!strcasecmp (fld->name, "Macro"))
		field->field_macro      = 
		  xmlNodeListGetString (doc, fld->childs, 1);
	      if (!strcasecmp (fld->name, "Default"))
		field->field_default    = 
		  xmlNodeListGetString (doc, fld->childs, 1);
	      if (!strcasecmp (fld->name, "ToolTip"))
		field->field_tip        =
		  xmlNodeListGetString (doc, fld->childs, 1);
	      if (!strcasecmp (fld->name, "Comment"))
		field->field_comment    = 
		  xmlNodeListGetString (doc, fld->childs, 1);
	      fld = fld->next;
	    }
	  form->field_list = g_slist_append (form->field_list, field);
	}
      cur = cur->next;
    }
  return form;
}
/****************************************************************************
 * WriteXmlFormData ()
 *
 * Saves the data from a form as an xml file for later use.
 ***************************************************************************/
gboolean
WriteXmlFormData (forms_def *form, GSList *field_list)
{
  return FALSE;
}

/* EOF */


