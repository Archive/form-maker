/* -*- Mode: C -*-
 * $Id$
 * Form Maker  -- A premade form filler outer.
 * Copyright 1999 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * This widget implements the main chooser window listing all forms avaliable
 * and the groups they are in. This is only used in the interactive mode.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <gnome.h>
#include "form_window.h"
#include "entry_window.h"

#include "debug.h"

/****************************************************************************
 * Forward declarations
 ***************************************************************************/
static void       form_window_class_init       (form_window_class   *klass);
static void       form_window_init             (form_window         *window);
static void       form_list_cb                 (GtkWidget           *widget,
						GdkEventButton      *event,
						gpointer            data);
static void       form_complete_button_cb      (GtkWidget           *widget,
						gpointer            data);
/**
 * form_window_get_type:
 *
 **/
GtkType 
form_window_get_type ()
{
  static GtkType window_type = 0;
  if (!window_type)
    {
      GtkTypeInfo window_info = 
      {
	"form_window",
	sizeof (form_window),
	sizeof (form_window_class),
	(GtkClassInitFunc) form_window_class_init,
	(GtkObjectInitFunc) form_window_init,
	/* reserved 1 */ NULL,
	/* reserved 2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      window_type = gtk_type_unique (gnome_app_get_type (), &window_info);
    }
  return window_type;
}
/****************************************************************************
 * form_window_class_init ()
 ***************************************************************************/
static void
form_window_class_init (form_window_class *klass)
{
}
/****************************************************************************
 * form_window_init ()
 ***************************************************************************/
static void
form_window_init (form_window *window)
{
  GtkWidget   *window_vbox;
  GtkWidget   *window_frame;
  GtkWidget   *window_table;
  GtkWidget   *button;

  window->form_list     = NULL;
  window->selected_form = NULL;
  window->form_groups   = g_hash_table_new (g_str_hash, g_str_equal);

  gnome_app_construct (GNOME_APP (window), "Form_Maker", 
		       _("Automated Form Generator"));
  /*
   * Build any menus
   */
  menu_create (window);
  /*
   * Build the window.
   */
  window_table = gtk_table_new (2, 2, FALSE);
  gtk_container_set_border_width (GTK_CONTAINER (window_table), 2);
  window_frame = gtk_frame_new (_("Available Forms"));
  gtk_container_border_width (GTK_CONTAINER (window_frame), 2);
  gtk_table_attach (GTK_TABLE (window_table), window_frame,
		    0, 1, 0, 2,
		    GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 0, 0);

  window->form_notebook = gtk_notebook_new ();
  gtk_container_add (GTK_CONTAINER (window_frame), window->form_notebook);
  gtk_notebook_popup_enable (GTK_NOTEBOOK (window->form_notebook));
  /*
   * Now the buttons down the side.
   */
  window_vbox = gtk_vbutton_box_new ();
  gtk_table_attach (GTK_TABLE (window_table), window_vbox, 
		    1, 2, 0, 1,
		    GTK_SHRINK , GTK_SHRINK, 0, 0);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (window_vbox), 
			     GTK_BUTTONBOX_START);
  window->complete_button = gtk_button_new_with_label(_("Complete Current Form"));
  gtk_signal_connect (GTK_OBJECT (window->complete_button), "clicked",
		      (GtkSignalFunc) form_complete_button_cb,
		      window);
  gtk_container_add (GTK_CONTAINER (window_vbox), window->complete_button);
  button = gtk_button_new_with_label (_("Add Form Definition"));
  gtk_container_add (GTK_CONTAINER (window_vbox), button);
  window->edit_button = gtk_button_new_with_label (_("Edit Form Definition"));
  gtk_container_add (GTK_CONTAINER (window_vbox), window->edit_button);
  window->delete_button = gtk_button_new_with_label (_("Delete Form Definition"));
  gtk_container_add (GTK_CONTAINER (window_vbox), window->delete_button);
  
  /*
   * Show the whole mess and add it to the gnome_app
   */
  gtk_widget_show_all (window_table);
  gnome_app_set_contents (GNOME_APP (window), window_table);

}
/***************************************************************************
 * form_window_new ()  -- PUBLIC
 ***************************************************************************/
GtkWidget *
form_window_new (const gchar *geometry)
{
  return GTK_WIDGET (gtk_type_new (FORM_WINDOW_TYPE));
}
/****************************************************************************
 * form_window_add_form () -- PUBLIC
 ***************************************************************************/
void
form_window_add_form (form_window *window, forms_def *form)
{
  GtkWidget *scrolled;
  GtkWidget *list;
  GtkWidget *list_item;
  GList     *children;

  gint       c_width, c_height;
  g_return_if_fail (form != NULL);
  if (!form->group)
    {
      /* Something messed up in the parsing no group? Assign to the default.
       */
      form->group = g_strdup ("Default");
    }
  if((scrolled = g_hash_table_lookup (window->form_groups, form->group)))
    {
      /* ICK! */
      children = gtk_container_children (GTK_CONTAINER (scrolled));
      list = children->data;
      children = gtk_container_children (GTK_CONTAINER (list));
      list = children->data;
    }
  else
    {
      GtkWidget   *label;
      gchar       *buf;
      
      buf           = g_strdup_printf ("%s %s", form->group, "Forms");
      label         = gtk_label_new (buf);
      gtk_misc_set_padding (GTK_MISC (label), 5, 0);
      g_free (buf);
      c_width  = gdk_string_width (label->style->font, "xW") / 2;
      c_height = label->style->font->ascent +
	         label->style->font->descent;
      
      scrolled = gtk_scrolled_window_new (NULL, NULL);
      gtk_container_set_border_width (GTK_CONTAINER (scrolled), 10);
      gtk_widget_set_usize (scrolled, c_width * 80, c_height * 24);
      gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
				      GTK_POLICY_AUTOMATIC,
				      GTK_POLICY_ALWAYS);
      list = gtk_list_new ();
      gtk_list_set_selection_mode (GTK_LIST (list), GTK_SELECTION_SINGLE);
      gtk_list_set_selection_mode (GTK_LIST (list), GTK_SELECTION_BROWSE);
      gtk_signal_connect (GTK_OBJECT (list), "button_press_event",
			  (GtkSignalFunc) form_list_cb,
			  window);
      gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (scrolled),
					     list);
      gtk_notebook_append_page (GTK_NOTEBOOK (window->form_notebook), 
				scrolled, label);
      gtk_widget_show_all (scrolled);
      g_hash_table_insert (window->form_groups, form->group, scrolled);
    }
  g_assert (list != NULL);
  list_item = gtk_list_item_new_with_label (form->name);
  gtk_object_set_data (GTK_OBJECT (list_item), "form", form);
  gtk_container_add (GTK_CONTAINER (list), list_item);
  gtk_widget_show (list_item);
}
/****************************************************************************
 * Callback functions.
 ***************************************************************************/
static void 
form_list_cb (GtkWidget *widget, GdkEventButton *event, gpointer data)
{
  g_return_if_fail (data != NULL);
  g_return_if_fail (IS_FORM_WINDOW (data));
  
  switch (event->type)
    {
    case GDK_BUTTON_PRESS:
      break;
    case GDK_2BUTTON_PRESS:
      gtk_widget_show (GTK_WIDGET (entry_window_new 
				   (form_window_get_current_form 
				    (FORM_WINDOW (data)))));
      break;
    default:
      break;
    }
}

static void
form_complete_button_cb (GtkWidget *widget, gpointer data)
{
  gtk_widget_show (GTK_WIDGET (entry_window_new (form_window_get_current_form 
						 (FORM_WINDOW (data)))));
}

/****************************************************************************
 * form_window_del_form () -- PUBLIC
 ***************************************************************************/
void
form_window_del_form (form_window *window, forms_def *form)
{
}
/****************************************************************************
 * form_window_get_current_form () -- PUBLIC
 ***************************************************************************/
forms_def *
form_window_get_current_form (form_window *window)
{
  GList           *list;
  GtkWidget       *item;
  GtkObject       *list_item;
  forms_def       *form;
  GtkNotebookPage *cur_page;

  g_return_val_if_fail (window != NULL, NULL);
  g_return_val_if_fail (IS_FORM_WINDOW (window), NULL);

  cur_page = GTK_NOTEBOOK (window->form_notebook)->cur_page;
  if (!cur_page)
    return NULL;
  /* Get the scrolled window widget */
  item     = cur_page->child;
  list     = gtk_container_children (GTK_CONTAINER (item));
  /* Now the viewport */
  item     = list->data;
  list     = gtk_container_children (GTK_CONTAINER (item));
  /* NOW the actual list.. */
  item     = list->data;
  
  list     = GTK_LIST (item)->selection;
  list_item= GTK_OBJECT (list->data);
  form     = gtk_object_get_data (GTK_OBJECT (list_item), "form");

  return form;
}
/* EOF */
