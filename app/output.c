/* -*- Mode: C -*-
 * Copyright 1999 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Output the completed form.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/file.h>
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#include <sys/errno.h>

#include "worksheet.h"
#include "output.h"
#include "dialog.h"
#include "xml-io.h"
#include "gtkprintdialog.h"

#include "debug.h"

char out_filename[1024];
extern char *vals[];
extern int errno;

/*
 * Forward declarations
 */
/****************************************************************************
 * writeProlog ()
 ***************************************************************************/
void
writeProlog (forms_def *form, FILE *out_fd)
{
  time_t      t;
  D_FUNC_START;
  fprintf (out_fd, "%%!PS-Adobe-2.0 EPSF-2.0\n");
  fprintf (out_fd, "%%%%Creator: form-maker " FORMMAKER_VERSION "\n");
  fprintf (out_fd, "%%%%Title: %s\n", form->name);
  t = time(0);
  fprintf (out_fd, "%%%%CreationDate: %s", ctime(&t));
  fprintf (out_fd, "%%%%Origin: 0 0\n");
  fprintf (out_fd, "%%%%Pages: 1 +1\n");
  fprintf (out_fd, "%%%%EndComments\n");
  fprintf (out_fd, "%%%%BeginProlog\n");
  fprintf (out_fd, "100 dict begin\n");
  D_FUNC_END;
}
/****************************************************************************
 * writeWordWrap ()
 *
 * This will take a long macro and break it up into a multi-line macro
 * 
 ***************************************************************************/
void
writeWordWrap (FILE *out_fd)
{
  D_FUNC_START;
  fprintf (out_fd, "/wordbreak ( ) def\n");
  fprintf (out_fd, "/BreakIntoLines\n");
  fprintf (out_fd, "{ /proc exch def\n");
  fprintf (out_fd, "  /linewidth exch def\n");
  fprintf (out_fd, "  /textstring exch def\n");
  fprintf (out_fd, "  /breakwidth wordbreak stringwidth pop def\n");
  fprintf (out_fd, "  /curwidth 0 def\n");
  fprintf (out_fd, "  /lastwordbreak 0 def\n");
  fprintf (out_fd, "  /startchar 0 def\n");
  fprintf (out_fd, "  /restoftext textstring def\n");
  fprintf (out_fd, "  { restoftext wordbreak search\n");
  fprintf (out_fd, "    { /nextword exch def pop\n");
  fprintf (out_fd, "      /restoftext exch def\n");
  fprintf (out_fd, "      /wordwidth nextword stringwidth pop def\n");
  fprintf (out_fd, "      curwidth wordwidth add linewidth gt\n");
  fprintf (out_fd, "      { textstring startchar\n");
  fprintf (out_fd, "        lastwordbreak startchar sub\n");
  fprintf (out_fd, "        getinterval proc\n");
  fprintf (out_fd, "        /startchar lastwordbreak def\n");
  fprintf (out_fd, "       /curwidth wordwidth breakwidth add def }\n");
  fprintf (out_fd, "      { /curwidth curwidth wordwidth add\n");
  fprintf (out_fd, "        breakwidth add def\n");
  fprintf (out_fd, "      } ifelse\n");
  fprintf (out_fd, "      /lastwordbreak lastwordbreak\n");
  fprintf (out_fd, "      nextword length add 1 add def\n");
  fprintf (out_fd, "    }\n");
  fprintf (out_fd, "    { pop exit }\n");
  fprintf (out_fd, "    ifelse\n");
  fprintf (out_fd, "  } loop\n");
  fprintf (out_fd, "  /lastchar textstring length def\n");
  fprintf (out_fd, "  textstring startchar lastchar startchar sub\n");
  fprintf (out_fd, "  getinterval proc\n");
  fprintf (out_fd, "} def\n");
  D_FUNC_END;
}
/****************************************************************************
 * emitFied ()
 *
 * Take a given field from the form and emit the postscript macro for it.
 ***************************************************************************/
void
emitField (wks_data *datum, FILE *out_fd)
{
  const char    *cp;
  D_FUNC_START;
  fprintf (out_fd, "/%s (", datum->field_name);
  for (cp = datum->field_value; *cp; cp++)
    {
      if (*cp == '(' || *cp == ')' || *cp == '\\')
	putc ('\\', out_fd);
      putc (*cp, out_fd);
    }
  fprintf (out_fd, ") def\n");
  D_FUNC_END;
}

void
makeworksheet (GSList *field_list, forms_def *form, GtkPrinterDef *printer, 
	       gboolean gui)
{
   int         fd;
   FILE        *out_fd;
   struct stat fs, os;
   wks_data    *datum;
   time_t      t;
   char        buf[16*1024];
   int         n, rd;
   GtkWidget   *progress;
   gfloat      percent;
   D_FUNC_START;
   g_return_if_fail (form != NULL);
   g_return_if_fail (field_list != NULL);
   d_print (DEBUG_DUMP, "Opening template file (%s)\n", form->path);
   fd = open_template (form->path, gui);
   if (fd < 0)
     return;
   fstat (fd, &fs);
   rd = 0;
   d_print (DEBUG_DUMP, "Opening output file\n");
   out_fd = open_output (printer, gui);
   if (out_fd == NULL)
     return;
   if (gui)
     {
       progress = printing_dialog_open ();
       gtk_sync_display ();
     }
   writeProlog (form, out_fd);
   writeWordWrap (out_fd);
   while (field_list)
     {
       datum = field_list->data;
       emitField (datum, out_fd);
       gtk_sync_display ();
       field_list = field_list->next;
     }
   fprintf (out_fd, "%%%%EndProlog\n");
   fprintf (out_fd, "%%%%Page: \"1\" 1\n");
   while ((n = read(fd, buf, sizeof (buf))) > 0)
     {
       rd = rd + n;
       percent = (rd * 100.0) / (fs.st_size * 100.0);
       if (gui)
	 {
	   printing_dialog_update (GTK_WIDGET (progress), percent);
	   gtk_sync_display ();

	 }
       fwrite(buf, n, 1, out_fd);
       fflush (out_fd);
     }
   close (fd);
   fflush (out_fd);
   fprintf (out_fd, "end\n");
   fclose (out_fd);
   g_slist_free (field_list);
   switch (printer->print_destination)
     {
     case GTK_PRINT_PRINTER:
       snprintf (buf, sizeof (buf), "%s %s", printer->output_command, out_filename);
       d_print (DEBUG_DUMP, "Printer output selected, executing command: %s\n",
		buf);
       system (buf);
       if (gui)
	 unlink (out_filename); 
       break;
     case GTK_PRINT_FILE:
       break;
     }
   if (gui)
     printing_dialog_close (progress);
   D_FUNC_END;
}

/*
 * Form config saving.
 */
void
save_form_config (const gchar *pathname, forms_def *form)
{
  struct stat   st_buf;
  gboolean      overwrite;

  if (stat (pathname, &st_buf) == 0)
    {
      if (confirmation_dialog ("The \"%s\" file exists.\nOverwrite it?",
				       pathname) )
	{
	  WriteXmlForm (form, pathname);
	}
      else
	{
	  return;
	}
    }
  else
    {
      switch (errno)
	{
	case EBADF:
	case EFAULT:
	case ENOMEM:
	  g_print ("odd stat error?? \n");
	  return;
	  break;
	case EACCES:
	  g_print ("Permission denied.\n");
	  return;
	  break;
	case ENAMETOOLONG:
	  g_print ("Filename to long\n");
	  return;
	  break;
	}
      WriteXmlForm (form, pathname);
    }
}

/*
 * Function    : open_template
 * Description : This function is resposible for opening a template file
 *               and returning the file handle. This function also handles
 *               realitive/absolute problem. Absolute filenames are with
 *               a leading "/" while the realitive filenames will not.
 *               realitive filenames will be built up upon the TEMPLATE_PATH
 *               default.
 * Arguments   : char*   -- The filename to open.
 *               gui     -- A boolean indicateing weather to use a gui or not.
 * Returns     : -1 on fail (and errno should be set via the syscalls)
 *                or the filehandle of the open file.
 */
gint 
open_template (gchar *file, gboolean gui)
{ 
  gint         ret_fd;
  struct stat fd_stat;

  g_return_val_if_fail (file != NULL, -1);

  if (*file = '/')
    {
      if (stat (file, &fd_stat) < 0)
	{
	  g_warning ("OPEN TEMPLATE:stat failure.\n(%s)\n", file);
	  return -1;
	}
      if (S_ISREG (fd_stat.st_mode))
	{
	  ret_fd = open (file, O_RDONLY);
	  return ret_fd;
	}
      return -1;
    }
  else
    {
      /*
       * FIXME: Figure out how to do/handle this.
       */
      g_print ("Relative filename passed (%s)\n", file);
    }
}

/****************************************************************************
 * Function   : open_output
 * Description: This function will open the output file handle depending
 *              on whether printer or file output was selected.
 * Arguments  : printer -- A pointer to a printer description structure.
 *              gui     -- Boolean as to weather to use a gui or not.
 * Returns    : FILE * on success, NULL on failure.
 ***************************************************************************/
FILE *
open_output (GtkPrinterDef *printer, gboolean gui)
{
  FILE         *ret_fd;
  char         *tmp_dir;
  struct stat  fd_stat;
  pid_t        pid;
  time_t       tim;

  g_return_val_if_fail (printer != NULL, NULL);

  pid = getpid ();
  tim = time (0);
  switch (printer->print_destination)
    {
    case GTK_PRINT_PRINTER:
      if (getenv ("TMPDIR"))
	{
	  tmp_dir = g_strdup (getenv ("TMPDIR"));
	}
      else
	{
	  tmp_dir = g_strdup ("/tmp/");
	}
      snprintf (out_filename, sizeof(out_filename), "%s%x%d.ps", 
		tmp_dir, pid, tim);
      return (fopen (out_filename, "w"));
      break;
    case GTK_PRINT_FILE:
      if (!gui)
	{
	  return (fopen (printer->output_filename, "w"));
	}
      if (!(stat (printer->output_filename, &fd_stat)))
	{
	  if (confirmation_dialog
	      ("Please confirm:\nThe file \"%s\" already exists.\n"
	       "Ok to overwrite it?", printer->output_filename))
	    {
	      return (fopen (printer->output_filename, "w"));
	    }
	  return NULL;
	}
      return (fopen (printer->output_filename, "w"));
      break;
    }
  return NULL;   /* not reached */
}

/*
 * Non interactive output.
 */
void
non_interactive_output (forms_def *form, char *data_file, char *end_command)
{
  GtkPrinterDef   *printer;
  GSList          *field_datum = NULL;
  wks_data        *datum;
  int             flum;
  char            buf[2048];
  char            mac_buf[80];
  FILE            *in_fd;
  struct stat     fd_stat;
  int             cnt;

  printer = (GtkPrinterDef *)g_malloc (sizeof (GtkPrinterDef));
  if (end_command)
    printer->output_command = g_strdup (end_command);
  else
    printer->output_command = g_strdup ("lpr");
  printer->output_filename = g_strdup ("/tmp/testing.ps");  /* FIXME */
  printer->print_destination = GTK_PRINT_FILE;
  if (*data_file = '/')
    {
      if (stat (data_file, &fd_stat) < 0)
	{
	  g_print ("stat failure on the data file.\n");
	  return;
	}
      if (S_ISREG (fd_stat.st_mode))
	{
	  in_fd = fopen (data_file, "r");
	  while (!feof (in_fd))
	    {
	      fscanf (in_fd, "%s", &mac_buf);
	      cnt = 0;
	      if (feof (in_fd))
		break;
	      while ( (flum = fgetc (in_fd)) != EOF)
		{
		  buf[cnt] = flum;
		  if (flum == '|')
		    {
		      buf[cnt] = '\0';
		      cnt = 0;
		      break;
		    }
		  cnt++;
		}
	      datum = (wks_data *)g_malloc (sizeof (wks_data));
	      datum->field_name = g_strdup (mac_buf);
	      datum->field_value = g_strdup (buf);
	      field_datum = g_slist_append (field_datum, datum);
	    }
	  makeworksheet (field_datum, form, printer, FALSE);
	}
    }
}

/* EOF */


