/* -*- Mode: C -*-
 * Copyright (C) 1998 Comstar Communications Inc.
 * Author: Gregory McLean <gregm@comstar.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */
#ifndef __FIELD_EDIT_H__
#define __FIELD_EDIT_H__

#include <gtk/gtk.h>

#include "worksheet.h"

/****************************************************************************
 * Widget Macros
 ***************************************************************************/
#define FIELD_EDIT_TYPE (field_edit_get_type ())
#define FIELD_EDIT(obj) \
        GTK_CHECK_CAST ((obj), FIELD_EDIT_TYPE, field_edit_panel)
#define FIELD_EDIT_CLASS(klass) \
        GTK_CHECK_CLASS_CAST ((klass), FIELD_EDIT_TYPE, field_edit_panelClass)
#define IS_FIELD_EDIT(obj) \
        GTK_CHECK_TYPE ((obj), FIELD_EDIT_TYPE)
#define IS_FIELD_EDIT_CLASS(klass) \
        GTK_CHECK_TYPE ((klass), FIELD_EDIT_TYPE)
/****************************************************************************
 * Control Blocks
 ***************************************************************************/
typedef struct _field_edit_panel               field_edit_panel;
typedef struct _field_edit_panelClass          field_edit_panelClass;
struct _field_edit_panel
{
  GtkVBox       vbox;

  GtkWidget     *field_name;
  GtkWidget     *field_macro;
  GtkWidget     *field_default;
  GtkWidget     *field_tooltip;
  GtkWidget     *field_type;
  GtkWidget     *field_max_size;

  field_def     *field;
};
struct _field_edit_panelClass
{
  GtkVBoxClass  parent_class;
  /* The signals we implement */
  void      (* changed)     (field_edit_panel   *panel);  /* Changed */
};
/****************************************************************************
 * Public API
 ***************************************************************************/
GtkType      field_edit_get_type              (void);
GtkWidget    *field_edit_new                  (void);
void         field_edit_put_data              (field_edit_panel  *panel,
					       field_def         *field);
gboolean     field_edit_get_data              (field_edit_panel  *panel,
					       field_def         *field);
#endif
/* EOF */

