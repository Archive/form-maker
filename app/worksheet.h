/* -*- Mode: C -*-
 * $Id$
 * Copyright 1999 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */
#ifndef __WORKSHEET_H__
#define __WORKSHEET_H__
#include <glib.h>

#define ELEMENTS(x)                (sizeof (x) / sizeof (x[0]))

/*
 * This structure will hold all the datum collected for a worksheet
 */
typedef struct __wks_data {
  char                *field_name;
  char                *field_value;
} wks_data;

/*
 * Types for the field datum
 */
enum {
  FIELD_TEXT_ENTRY,
  FIELD_NUMS_ENTRY,                    /* Spin button entry */
  FIELD_NUML_ENTRY,                    /* slider entry */
  FIELD_DATE_ENTRY,
  FIELD_ENTRY_END                      /* End of entry types */
};

/*
 * This holds the definition of a form's field
 */
typedef struct __field_def {
  gchar    *field_macro;          /* The macro to generate */
  gchar    *field_name;         /* What to show on the screen */
  gchar    *field_default;       /* The default value */
  gchar    *field_tip;           /* Tip for this field */
  gchar    *field_comment;       /* The comment for this field */
  gint     field_type;           /* Type of field */
  gint     field_width;          /* How big the field can be */
  gint     min, max;             /* Minimum and max values for the numeric */
  gboolean (*vailidator) ();     /* Call this function to vaildate the data */
  gpointer valid_magic;          /* This gets passed to the above function */
} field_def;

/*
 * This holds the definition of the form itself
 */
typedef struct __forms_def {
  gchar     *name;                /* Name of the form */
  gchar     *path;                /* Where to find the template file */
  gchar     *xml;                 /* The xml template for data entry screen */
  gchar     *cfg_path;            /* Where the template config was read from */
  gchar     *group;               /* Which group this form belongs to */
  gint      field_count;          /* Used in the parser */
  GSList    *field_list;          /* List of fields. */
} forms_def;

/*
 * This holds the grouping of a form
 */
typedef struct __form_grp {
  gchar     *group;
  GSList    *forms;
}form_grp;

void          open_form_panel          (forms_def         *form);

#endif

/* EOF */
