/* -*- Mode: C -*-
 * $Id$
 * Copyright 1999, Gregory McLean 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * xml-io.h: Interfaces to save/read form maker form definitions and form
 *           data using XML encoding. This is based on the one found in
 *           gnumeric written by Daniel Veillard <Daniel.Veillard@w3.org>
 */
#ifndef ___XML___
#define ___XML___

forms_def      *ReadXmlForm               (const char *filename);
gboolean       WriteXmlForm               (forms_def  *form,
					   const char *filename);
gboolean       WriteXmlFormData           (forms_def  *form, 
					   GSList     *field_list);
void           xml_init                   (void);

#endif 
/* EOF */
