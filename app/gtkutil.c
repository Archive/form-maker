/* -*- Mode: C -*-
 * Copyright 1998 Gregory McLean
 * Author: Gregory McLean <gregm@comstar.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Misc gtk util functions common to all files
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include "worksheet.h"
#include "gtkutil.h"

#include "debug.h"

#define NOTE_LAB_PADDING         30

static void           destroy_hash_element       (void     *key,
						  void     *val,
						  void     *data);
#ifdef DEBUG_HASH
static void           print_hash_element         (void     *key,
						  void     *val,
						  void     *data);
#endif
static void           note_hash_element          (void     *key,
						  void     *val,
						  void     *data);
static void           group_hash_element         (void     *key,
						  void     *val,
						  void     *data);
static void           check_hash                 (void);
void                  list_signal_cb             (GtkWidget *widget,
						  gpointer data);
void                  page_switch                (GtkWidget       *widget,
						  GtkNotebookPage *page,
						  gint            page_num);
GHashTable *form_hash = NULL;
GList      *group_list = NULL;
/*
 * Function    : get_widget
 */
GtkWidget *
get_widget (GtkWidget  *widget, const gchar *name)
{
   GtkWidget *found;

   if (widget->parent)
     widget = gtk_widget_get_toplevel (widget);
   found = gtk_object_get_data (GTK_OBJECT (widget), name);
   if (!found)
     {
       g_warning ("Widget not found: %s", name);
       return NULL;
     }
   return found;
}

/*
 * Hash table functions
 */


void
destroy_form_hash (void)
{
  if (!form_hash)
    return;
  g_hash_table_foreach (form_hash, destroy_hash_element, 0);
  g_hash_table_destroy (form_hash);
  form_hash = NULL;
}

form_grp *
find_form_group (const gchar *group)
{
  return (form_grp *)(g_hash_table_lookup (form_hash, group));
}

void 
add_form_to_group (gchar *group, forms_def *form)
{
  form_grp   *new_entry;

  check_hash ();
  new_entry = g_hash_table_lookup (form_hash, group);
  if (!new_entry)
    {
      new_entry = g_new0 (form_grp, 1);
      new_entry->group = g_strdup (group);
      new_entry->forms = g_slist_append (new_entry->forms, form);
      g_hash_table_insert (form_hash, g_strdup(group), new_entry);
    }
  else
      new_entry->forms = g_slist_append (new_entry->forms, form);
#ifdef DEBUG_HASH
  g_hash_table_foreach (form_hash, print_hash_element, 0);
#endif
}

void
build_form_notebook (GtkWidget *window)
{
  g_hash_table_foreach (form_hash, note_hash_element, window);
}

void 
build_group_list (GtkWidget *combo)
{
  g_hash_table_foreach (form_hash, group_hash_element, combo);
  gtk_combo_set_popdown_strings (GTK_COMBO (combo), group_list);
}

void 
free_group_list ()
{
  g_list_free (group_list);
  group_list = NULL;
}

static void 
group_hash_element (void *key, void *val, void *data)
{
  form_grp      *tmp = val;
  
  group_list = g_list_append (group_list, tmp->group);
  g_print ("Group %s\n", tmp->group);
}

static void 
note_hash_element (void *key, void *val, void *data)
{
  form_grp        *tmp = val;
  GtkWidget       *notebook;
  GtkWidget       *label;
  GtkWidget       *scrolled;
  GtkWidget       *list;
  GtkNotebookPage *cur_page;
  gchar            buf[1024];

  notebook = get_widget (GTK_WIDGET (data), "form_notebook");
  snprintf (buf, sizeof (buf), "%s %s", tmp->group, "Forms");
  label = gtk_label_new (buf);
  gtk_misc_set_padding (GTK_MISC (label), NOTE_LAB_PADDING, 0);
  gtk_misc_set_alignment (GTK_MISC (label), 0.1, 0.5);
  gtk_object_set_data (GTK_OBJECT (label), "label_group", tmp->group);
  scrolled = gtk_scrolled_window_new (NULL, NULL);
  gtk_container_border_width (GTK_CONTAINER (scrolled), 10);
  gtk_widget_set_usize (GTK_WIDGET (scrolled), 250, 200);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
				  GTK_POLICY_AUTOMATIC, 
				  GTK_POLICY_ALWAYS);
  gtk_notebook_append_page (GTK_NOTEBOOK (notebook), scrolled,
			    label);
  cur_page = GTK_NOTEBOOK (notebook)->cur_page;

  list = gtk_list_new ();
  gtk_object_set_data (GTK_OBJECT (data), buf, list);
  gtk_object_set_data (GTK_OBJECT (label), "list_obj", list);
  gtk_object_set_data (GTK_OBJECT (list), "group", tmp->group);
  gtk_list_set_selection_mode (GTK_LIST (list), GTK_SELECTION_SINGLE);
  gtk_list_set_selection_mode (GTK_LIST (list), GTK_SELECTION_BROWSE);
  gtk_signal_connect (GTK_OBJECT (list), "button_press_event",
		      (GtkSignalFunc) (list_signal_cb),
		       data);
  gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (scrolled),
					 list);

}	      

#ifdef DEBUG_HASH 
static void
print_hash_element (void *key, void *val, void *data)
{
  form_grp   *tmp = val;
  GSList     *list;
  forms_def  *form;

  list = tmp->forms;
  while (list)
    {
      form = list->data;
      g_print ("In the hash table! (%s)\n", form->name);
      list = list->next;
    }
}
#endif

static void 
destroy_hash_element (void *key, void *val, void *data)
{
  form_grp   *tmp = val;

  g_slist_free (tmp->forms);
  g_free (key);
}

static void
check_hash (void)
{
  if (!form_hash)
    form_hash = g_hash_table_new (g_str_hash, g_str_equal);
}

void
page_switch (GtkWidget *widget, GtkNotebookPage *page, gint page_num)
{
  g_print ("Page is %d\n", page_num);
}
/****************************************************************************
 *
 *  Function to update a field.  What a pain!
 *
 *  old_val is a pointer to the field pointer in the record.  The contents of
 *  the field pointer may be NULL, the field pointer may be zero length.
 *
 *  new_val is a reference pointer to the new value.  It may not be NULL. It
 *  may point to a zero length string however.
 *
 *  The new_val field is stripped of leading and trailing blanks.  If the
 *  result is a zero length string, the zero length string is converted
 *  into a NULL.
 *
 *  If the
 *
 *  Returns TRUE if the field changed,  false otherwise.
 *
 ***************************************************************************/
gboolean
update_string_field (gchar ** old_val, gchar * new_val)
{
  gchar * new_copy;

  new_copy = g_strstrip (g_strdup (new_val)); /* Copy the new value */

  if (!strlen(new_copy))        /* If, after stripping leading and trailing */
    {                           /* blanks, the result is zero length, then */
      g_free (new_copy);        /* replace the zero length string with a */
      new_copy = NULL;          /* NULL. */
    }

  if (!*old_val && !new_copy)    /* If the old value was NULL, and the new */
    return FALSE;               /* value is NULL, the field did not change */

  if (!*old_val)                 /* If the old value was NULL, and the new */
    {                           /* value is non-zero-length, then replace */
      *old_val = new_copy;       /* the NULL with the new field value, and */
      return TRUE;              /* indicate that the field changed. */
    }

  if (!new_copy)                /* If the old value was not NULL, and the */
    {                           /* new value is NULL, then free the old */
      g_free (*old_val);         /* data, store NULL as the new value, and */
      *old_val = NULL;           /* indicate that the field has changed */
      return TRUE;
    }

  if (!strcmp(*old_val, new_copy))       /* If the two strings compare equal */
    {
      g_free (new_copy);                /* Get rid of the identical string */
      return FALSE;                     /* The field did not change */
    }

  g_free (*old_val);             /* Get rid of the old string value */
  *old_val = new_copy;           /* Store the new string value */
  return TRUE;                  /* The field changed */
}
/**
 * gtk_sync_display:
 * 
 * Spin till all gtk events have been done.
 *
 **/
void
gtk_sync_display ()
{
  while (gtk_events_pending ())
    gtk_main_iteration ();
  gdk_flush();
}

/* EOF */


