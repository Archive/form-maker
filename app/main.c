/* -*- Mode: C -*-
 * Copyright 1999 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <sys/types.h>
#include <sys/file.h>
#include <stdlib.h>
#include <stdio.h>
#include <gnome.h>

#include <glade/glade.h>

#include "form_window.h"
#include "worksheet.h"
#include "output.h"
#include "formfile.h"
#include "chooser.h"
#include "paths.h"

#define __IN_MAIN_C__
#include "debug.h"
/***************************************************************************
 * Local functions.
 **/
static void       session_die             (GnomeClient        *client,
                                           gpointer           data);
static gint       save_session            (GnomeClient        *client,
                                           gint               phase,
                                           GnomeSaveStyle     save_style,
                                           gint               is_shutdown,
                                           GnomeInteractStyle interact_style,
                                           gint               is_fast,
                                           gpointer           client_data);

/****************************************************************************
 * Global data allocation.
 ***************************************************************************/
form_window    *app_window;
int            debug_level;
int            trace_indent;
static gchar   *geometry = NULL;
poptContext    pctx;
struct poptOption options[] = {
  { "geometry", '\0', POPT_ARG_STRING, &geometry, 0, 
    N_("Specify the geometry of the main window"),
    N_("GEOMETRY") },
  { NULL, '\0', 0, NULL, 0 }
};

/****************************************************************************
 * Main Program Entry Point.
 ***************************************************************************/
int
main (int argc, char *argv[])
{
  GList       *gl;
  forms_def   *form;
  gchar       *template_cfg;
  GnomeClient *client;

  bindtextdomain (PACKAGE, GNOMELOCALEDIR);
  textdomain (PACKAGE);

  gnome_init_with_popt_table (PACKAGE, VERSION, argc, argv, options, 0, NULL);
  template_cfg = g_strdup (TEMPLATE_CONF);
  
  /** connect up to the session manager */
  client = gnome_master_client ();
  gtk_signal_connect (GTK_OBJECT (client), "save_yourself",
		      GTK_SIGNAL_FUNC (save_session), argv[0]);
  gtk_signal_connect (GTK_OBJECT (client), "die", 
		      GTK_SIGNAL_FUNC (session_die), NULL);

  if (!parse_forms (template_cfg))
    {
      form = (forms_def *)g_malloc (sizeof (forms_def));
      form->name = g_strdup ("Blank Form");
      form->group = g_strdup ("Default group");
      form->field_list = NULL;
      parser_add_form (form);
      g_free (form);
    }
  
  /* 
   * Initilize libglade (used for custom entry areas)
   */
  glade_init();
  /*
   * And if we want to play with the layout engine
   */
#ifdef USE_GLE
  gle_init (&argc, &argv);
#endif
  app_window = form_window_new (geometry);
  gl = available_forms ();
  while (gl)
    {
      form = (forms_def *)gl->data;
      if (form)
	form_window_add_form (app_window, form);
      gl = gl->next;
    }
  gtk_widget_show (app_window);
  gtk_main ();
  return 1;
}

/** session mangement **/
/**
 * save_session:
 **/
static gint
save_session (GnomeClient *client, gint phase, GnomeSaveStyle save_style,
              gint is_shutdown, GnomeInteractStyle interact_style,
              gint is_fast, gpointer client_data)
{
  gchar **argv;
  guint argc;

  argv = g_malloc0(sizeof (gchar *)*4);
  argc = 1;

  argv[0] = client_data;
  /* add any addtional state info here. */
  gnome_client_set_clone_command (client, argc, argv);
  gnome_client_set_restart_command (client, argc, argv);
  return TRUE;
}

/**
 * session_die
 **/
static void
session_die (GnomeClient *client, gpointer client_data)
{
  gtk_main_quit ();
}

/* EOF */




