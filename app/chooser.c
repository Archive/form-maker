/* -*- Mode: C -*-
 * $Id$
 * Copyright (C) 1998 Gregory McLean 
 * Author: Gregory McLean <gregm@comstar.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * The forms chooser -- Obsolete
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <gnome.h>

#include "gtkutil.h"
#include "worksheet.h"
#include "chooser.h"
#include "editor.h"

#include "debug.h"
/****************************************************************************
 * Forward declarations
 ***************************************************************************/
void                  list_signal_cb            (GtkWidget       *widget,
						 GdkEventButton  *event,
						 gpointer        data);
static void           make_button_cb            (GtkWidget       *widget,
						 gpointer        data);
static void           add_button_cb             (GtkWidget       *widget,
						 gpointer        data);
static void           edit_button_cb            (GtkWidget       *widget,
						 gpointer        data);
static void           delete_button_cb          (GtkWidget       *widget,
                                                 gpointer        data);
static GSList         *form_list   = NULL;
extern GHashTable     *form_hash;
extern GtkWidget      *app_window;

void
chooser_panel ()
{
  GtkWidget     *window;
  GtkWidget     *label;
  GtkWidget     *table;
  GtkWidget     *bar;
  GtkWidget     *frame;
  GtkWidget     *b_box;
  GtkWidget     *button;
  GtkWidget     *form_notebook;

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), "Forms Generator");
  gtk_window_set_policy (GTK_WINDOW (window), TRUE, TRUE, FALSE);
  gtk_container_border_width (GTK_CONTAINER (window), 3);

  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		      GTK_SIGNAL_FUNC (gtk_main_quit), window);
  
  frame = gtk_frame_new (NULL);
  gtk_container_add (GTK_CONTAINER (window), frame);
  gtk_container_border_width (GTK_CONTAINER (frame), 2);

  table = gtk_table_new (5, 5, FALSE);
  gtk_container_border_width (GTK_CONTAINER (table), 2);
  gtk_container_add (GTK_CONTAINER (frame), table);
 
  label = gtk_label_new ("Available forms");
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.0);
  gtk_table_attach (GTK_TABLE (table), label,
		    0, 1, 0, 1,
		    GTK_FILL | GTK_EXPAND, GTK_FILL, 2, 1);
  form_notebook = gtk_notebook_new ();
  gtk_object_set_data (GTK_OBJECT (window), "form_notebook", form_notebook);

  gtk_table_attach (GTK_TABLE (table), form_notebook, 0, 3, 0, 3,
		    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_notebook_set_tab_border (GTK_NOTEBOOK (form_notebook), 1);
  build_form_notebook (window);

  bar = gtk_vseparator_new ();
  gtk_table_attach (GTK_TABLE (table), bar, 3, 4, 0, 3, GTK_FILL, GTK_FILL, 
		    4, 1);
  
  frame = gtk_frame_new (NULL);
  gtk_container_border_width (GTK_CONTAINER (frame), 2);
  gtk_table_attach (GTK_TABLE (table), frame, 4, 5, 0, 3,
		    GTK_FILL, GTK_FILL, 0, 0);
  b_box = gtk_vbutton_box_new ();
  gtk_container_add (GTK_CONTAINER (frame), b_box);
  gtk_container_border_width (GTK_CONTAINER (b_box), 2);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (b_box), GTK_BUTTONBOX_SPREAD);
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (b_box), 2);

  /*
   * Fill out form button 
   */
  button = gtk_button_new_with_label ("Complete form...");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) make_button_cb,
		      window);
  gtk_container_add (GTK_CONTAINER (b_box), button);

  /* 
   * Remove a form from the list
   */
  button = gtk_button_new_with_label ("Delete form...");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
                      (GtkSignalFunc) delete_button_cb,
                      window);
  gtk_container_add (GTK_CONTAINER (b_box), button);

  /*
   * Add a new form to the list
   */
  button = gtk_button_new_with_label ("Add form...");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) add_button_cb,
		      window);
  gtk_container_add (GTK_CONTAINER (b_box), button);

  /*
   * Edit a forms definition
   */
  button = gtk_button_new_with_label ("Edit form...");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) edit_button_cb,
		      window);
  gtk_container_add (GTK_CONTAINER (b_box), button);

  bar = gtk_hseparator_new ();
  gtk_table_attach (GTK_TABLE (table), bar, 0, 5, 3, 4,
		    GTK_FILL, GTK_FILL, 0, 3);

  frame = gtk_frame_new (NULL);
  gtk_container_border_width (GTK_CONTAINER (frame), 2);
  gtk_table_attach (GTK_TABLE (table), frame, 0, 5, 4, 5,
		    GTK_FILL, GTK_FILL, 0, 0);

  b_box = gtk_hbutton_box_new ();
  gtk_button_box_set_layout (GTK_BUTTON_BOX (b_box), GTK_BUTTONBOX_END);
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (b_box), 4);
  gtk_container_border_width (GTK_CONTAINER (b_box), 4);
  gtk_container_add (GTK_CONTAINER (frame), b_box);
  button = gtk_button_new_with_label ("Exit");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) gtk_main_quit, NULL);
  gtk_container_add (GTK_CONTAINER (b_box), button);
  fill_forms_list (window);
  gtk_widget_show_all (window);
}

void
form_list_add (forms_def *form, GtkWidget *list)
{
  GtkWidget    *listitem;

  listitem = gtk_list_item_new_with_label (form->name);
  gtk_object_set_data (GTK_OBJECT (listitem), "forms_def", (forms_def *) form);
   gtk_container_add (GTK_CONTAINER (list), listitem);
  gtk_widget_show (listitem);
}

void
fill_forms_list (GtkWidget *list)
{
  if (form_hash)
    g_hash_table_foreach (form_hash, form_hash_foreach, list);
}

void
form_hash_dump (void *key, void *val, void *data)
{
  form_grp    *tmp = val;
  forms_def   *form;
  GSList      *list;

  list = tmp->forms;
  while (list)
    {
      g_print ("Form is in %s group\n", tmp->group);
      form = (forms_def *)list->data;
      g_print ("Form is %s\n", form->name);
      list = list->next;
    }
}

void 
dump_forms_list ()
{
  if (form_hash)
    g_hash_table_foreach (form_hash, form_hash_dump, NULL);
}

void 
form_hash_foreach (void *key, void *val, void *data)
{
  form_grp   *tmp = val;
  forms_def  *form;
  GSList     *list;
  GtkWidget  *gtklist;
  gchar      buf[1024];

  list = tmp->forms;
  while (list)
    {
      snprintf (buf, sizeof(buf), "%s %s", tmp->group, "Forms");
      gtklist = get_widget (data, buf);
      form = list->data;
      /*      g_print ("%s\n", form->cfg_path); */
      form_window_add_form (app_window, form);
      form_list_add (form, gtklist);
      list = list->next;
    }
}

/*
 * Non interactive form (one at a time)
 */
forms_def * 
get_form_def ()
{
  GSList     *list;
  forms_def  *ret;

  list = g_slist_nth (form_list, 0);
  ret = (forms_def *)list->data;
  return ret;
}

void
parser_add_form (forms_def *form)
{
  forms_def  *foo;
  GSList     *list;
  field_def  *field;

  foo = (forms_def *)g_malloc (sizeof (forms_def));
  g_memmove ((char *)foo, (char *)form, sizeof (forms_def));
  foo->field_list = NULL;
  form_list = g_slist_append (form_list, foo);
  list = form->field_list;
  while (list)
    {
      field = (field_def *)g_malloc (sizeof (field_def));
      g_memmove ((char *)field, (char *)list->data, sizeof (field_def));
      foo->field_list = g_slist_append (foo->field_list, field);
      list = list->next;
    }
  add_form_to_group (foo->group, foo);
}

void
list_signal_cb (GtkWidget *widget, GdkEventButton *event, gpointer data)
{
  GList     *dlist;
  GtkObject *list_item;
  forms_def *form;

  dlist = GTK_LIST (widget)->selection;
  list_item = GTK_OBJECT (dlist->data);
  switch (event->type)
    {
    case GDK_BUTTON_PRESS:
      break;
    case GDK_2BUTTON_PRESS:
      form = (forms_def *)gtk_object_get_data (GTK_OBJECT (list_item),
					       "forms_def");
      if (form)
	open_form_panel (form);
      else
	g_print ("Err something went wrong\n");
      break;
    default:
      break;
    }
}

static void
make_button_cb (GtkWidget *widget, gpointer data)
{
  GtkObject        *list_item;
  GList            *dlist;
  forms_def        *form;
  GtkWidget        *notebook;
  GtkNotebookPage  *cur_page;

  notebook = get_widget (data, "form_notebook");
  cur_page = GTK_NOTEBOOK (notebook)->cur_page;
  dlist = GTK_LIST (gtk_object_get_data (GTK_OBJECT (cur_page->tab_label),
					 "list_obj"))->selection;
  
  if (dlist)
    {
      list_item = GTK_OBJECT (dlist->data);
      form = (forms_def *)gtk_object_get_data (GTK_OBJECT (list_item),
					       "forms_def");
      if (form->xml)
        open_xml_form_panel (form);
      else
	open_form_panel (form);
    }
  else
    {
      notice_dialog (FALSE, "No form was selected to be completed.");
    }
}

static void
add_button_cb (GtkWidget *widget, gpointer data)
{
  GtkWidget       *window;
  forms_def       *new;
  GtkWidget       *temp;
  GtkWidget       *dlist;
  GtkNotebookPage *cur_page;

  window = (GtkWidget *)form_panel ("Add a new form");
  gtk_object_set_data (GTK_OBJECT (window), "edit_form", (gboolean *)FALSE);
  cur_page = GTK_NOTEBOOK (get_widget (data, "form_notebook"))->cur_page;
  dlist = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (cur_page->tab_label),
					 "list_obj"));
  new = g_new0 (forms_def, 1);
  new->name = g_strdup ("<New form>");
  new->group = g_strdup ("<No Group>");
  parser_add_form (new);
  form_list_add (new, (GtkWidget *)dlist);
  gtk_object_set_data (GTK_OBJECT (window), "forms_def", new);
  temp = get_widget (window, "form_name");
  gtk_entry_set_text (GTK_ENTRY (temp), new->name);
  
}

static void
edit_button_cb (GtkWidget *widget, gpointer data)
{
  GtkWidget       *window;
  GtkWidget       *temp;
  GtkObject       *list_item;
  GList           *dlist;
  GList           *foo;
  forms_def       *form;
  field_def       *field;
  GtkWidget       *listitem;
  GtkNotebookPage *cur_page;

  window = GTK_WIDGET (form_panel ("Edit a form"));
  cur_page = GTK_NOTEBOOK (get_widget (data, "form_notebook"))->cur_page;
  dlist = GTK_LIST (gtk_object_get_data (GTK_OBJECT (cur_page->tab_label),
					 "list_obj"))->selection;
  if (dlist)
    {
      list_item = GTK_OBJECT (dlist->data);
      form = (forms_def *)gtk_object_get_data (GTK_OBJECT (list_item),
					       "forms_def");
      gtk_object_set_data (GTK_OBJECT (window), "forms_def", 
			   (forms_def *)form);
      gtk_object_set_data (GTK_OBJECT (window), "edit_form", 
			   (gboolean *)TRUE);
      temp = get_widget (window, "form_name");
      gtk_entry_set_text (GTK_ENTRY (temp), (form->name) ? form->name : "");
      temp = get_widget (window, "form_group");
      gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (temp)->entry), 
			  (form->group) ? form->group : "<None>");
      temp = get_widget (window, "template_path");
      gtk_entry_set_text (GTK_ENTRY (temp), (form->path) ? form->path : "");
      gtk_object_set_data (GTK_OBJECT (window), "file_name",
			   (gchar *)form->path);
      gtk_object_set_data (GTK_OBJECT (window), "print_file_name",
			   GTK_WIDGET (get_widget (window, "template_path")));
      gtk_entry_set_position (GTK_ENTRY (temp), 0);
      temp = get_widget (window, "field_list");
      foo = (GList *)(GTK_LIST (temp)->children);
      gtk_list_clear_items (GTK_LIST (temp), 0, g_list_length (foo));
      dlist = (GList *)form->field_list;
      while (dlist)
	{
	  field = (field_def *)dlist->data;
	  if (field)
	    {
	      listitem = gtk_list_item_new_with_label (field->field_name);
	      gtk_object_set_data (GTK_OBJECT (listitem), "field_def",
				   field);
	      gtk_container_add (GTK_CONTAINER (temp), listitem);
	      gtk_widget_show (listitem);
	      dlist = dlist->next;
	    }
	}
    }
  else
    {
      notice_dialog (FALSE, "You didn't select a form to edit.");
    }
  
}

static void
delete_button_cb (GtkWidget *widget, gpointer data)
{
    notice_dialog (FALSE, "Sorry Form deletion is not implemented yet.");
}
/* EOF */



