/*
 * Copyright 1998 Gregory McLean
 * Author: Gregory McLean <gregm@comstar.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Chooser.h
 */

#ifndef __CHOOSER_H__
#define __CHOOSER_H__
#include <config.h>
#include <gtk/gtk.h>

void          chooser_panel              (void);
void          form_list_add              (forms_def      *form,
					  GtkWidget      *list);
void          fill_forms_list            (GtkWidget      *list);
void          parser_add_form            (forms_def      *form);
void          form_hash_foreach          (void           *key, 
					  void           *val, 
					  void           *data);
#endif
/* EOF */
