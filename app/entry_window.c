/* -*- Mode: C -*-
 * $Id$
 * Form Maker  -- A premade form filler outer.
 * Copyright 1999 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * This widget implements the data entry window.
 * This is only used in the interactive mode.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <gnome.h>
#include "entry_window.h"

#include "debug.h"

/****************************************************************************
 * Forward declatations
 ***************************************************************************/
static void         entry_window_class_init      (entry_window_class  *klass);
static void         entry_window_init            (entry_window        *window);
static void         print_button_cb              (GtkWidget           *widget,
						  gpointer            data);
static void         clear_button_cb              (GtkWidget           *widget,
						  gpointer            data);
static void         close_button_cb              (GtkWidget           *widget,
						  gpointer            data);
static void
_connect (GladeXML *gui, const char *widget_name, const char *signal_name,
	  GtkSignalFunc callback, gpointer closure)
{
  gtk_signal_connect (
		      GTK_OBJECT (glade_xml_get_widget (gui, widget_name)),
		      signal_name,
		      callback, closure);
}
#define connect(a,b,c,d,e) _connect(a,b,c, GTK_SIGNAL_FUNC(d),e)

/****************************************************************************
 * entry_window_get_type
 ***************************************************************************/
GtkType
entry_window_get_type ()
{
  static GtkType window_type = 0;
  if (!window_type)
    {
      GtkTypeInfo window_info = 
      {
	"entry_window",
	sizeof (entry_window),
	sizeof (entry_window_class),
	(GtkClassInitFunc) entry_window_class_init,
	(GtkObjectInitFunc) entry_window_init,
	/* reserved 1 */ NULL,
	/* reserved 2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      window_type = gtk_type_unique (gnome_app_get_type (), &window_info);
    }
  return window_type;
}
/****************************************************************************
 * entry_window_class_init ()
 ***************************************************************************/
static void
entry_window_class_init (entry_window_class *klass)
{
}
/****************************************************************************
 * entry_window_init ()
 ***************************************************************************/
static void
entry_window_init (entry_window *window)
{
  gnome_app_construct (GNOME_APP (window), "Form_Maker", 
		      _("Complete Form"));

  form_menu_create (window);
}
/****************************************************************************
 * entry_window_new () -- PUBLIC
 ***************************************************************************/
GtkWidget *
entry_window_new (forms_def *form)
{
  entry_window   *window;
  gchar          *buf;

  window = gtk_type_new (ENTRY_WINDOW_TYPE);
  buf = g_strdup_printf ("%s %s", "Complete Form:", form->name);
  gtk_window_set_title (GTK_WINDOW (window), buf);
  window->form = form;
  if (form->xml)
    {
      window->xml = glade_xml_new (form->xml, "data_entry");
      if (!window->xml)
	{
	  g_error ("Failed to load xml interface description.");
	  return NULL;
	}
      gnome_app_set_contents (GNOME_APP (window), 
			      glade_xml_get_widget (window->xml, "data_entry"));
      clear_entry_window (GTK_WIDGET (window));
      connect (window->xml, "close_button", "clicked", 
	       close_button_cb, window);
      connect (window->xml, "clear_button", "clicked", 
	       clear_button_cb, window);
      connect (window->xml, "print_button", "clicked", 
	       print_button_cb, window);
    }
  else
    {
      GtkWidget   *label;
      GtkWidget   *entry;
      GtkWidget   *table;
      GtkWidget   *eventbox;
      GList       *gl;
      gint        i;
      
      gl = form->field_list;
      while (gl)
	{
	  gl = gl->next;
	}
      /* No xml form defined. Make a stab at doing a data entry screen, */
    }
  form_dialog_add (window, "Complete Form", window, buf);
  g_free (buf);
  return GTK_WIDGET (window);
}
/****************************************************************************
 * Misc support functions 
 ***************************************************************************/
void
clear_entry_window (GtkWidget *window)
{
  field_def    *field;
  forms_def    *form;
  GSList       *field_list;
  GtkWidget    *entry;

  g_return_if_fail (IS_ENTRY_WINDOW (window));
  field_list = (ENTRY_WINDOW (window)->form->field_list);
  while (field_list)
    {
      field = (field_def *)field_list->data;
      entry = glade_xml_get_widget (ENTRY_WINDOW (window)->xml, 
				    field->field_macro);
      if (entry)
	set_widget_text (entry, (field->field_default) ? 
			 field->field_default : "");
      field_list = field_list->next;
    }
}
	    
/****************************************************************************
 * Callbacks
 ***************************************************************************/
static void 
print_button_cb (GtkWidget *widget, gpointer data)
{
  gtk_widget_show_all (GTK_WIDGET (create_print_window (data)));
}

static void
clear_button_cb (GtkWidget *widget, gpointer data)
{
  clear_entry_window (GTK_WIDGET (data));
}

static void
close_button_cb (GtkWidget *widget, gpointer data)
{
  gtk_widget_destroy (GTK_WIDGET (data));
}

/* EOF */


 
