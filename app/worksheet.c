/* -*- Mode: C -*-
 * Copyright 1998 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Present the dialog for the form that will be filled out and printed.
 * redundant -- will be removed soon.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <sys/types.h>
#include <stdio.h>
#include <sys/file.h>
#include <sys/time.h>
#include <gnome.h>
#include <glade/glade.h>

#include "output.h"
#include "worksheet.h"
#include "gtkutil.h"
#include "editor.h"
#include "chooser.h"
#include "dialog.h"

#include "debug.h"

static void     print_button_cb               (GtkWidget      *widget,
					       gpointer       data);
static void     clear_button_cb               (GtkWidget      *widget,
					       gpointer       data);
static void     close_button_cb               (GtkWidget      *widget,
					       gpointer       data);

static void 
print_button_cb (GtkWidget *widget, gpointer data)
{
  gtk_widget_show_all (GTK_WIDGET (create_print_window (data)));
}

static void
clear_button_cb (GtkWidget *widget, gpointer data)
{
  forms_def   *form;
  field_def   *field;
  GSList      *field_list;
  GtkWidget   *parent;
  GtkWidget   *entry;
  gboolean    xml_form;
  GladeXML    *xml;
  char        buf[1024];

  parent = gtk_widget_get_toplevel (widget);
  if (!parent)
    return;
  if ( (xml_form = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (parent),
							 "is_xml_form"))) )
    xml = glade_get_widget_tree (parent);
  
  form = (forms_def *)gtk_object_get_data (GTK_OBJECT (parent), "forms_def");
  if (form)
    {
      field_list = form->field_list;
      while (field_list)
	{
	  field = (field_def *)field_list->data;
	  if (!(xml_form))
	    {
	      snprintf (buf, sizeof (buf), "%s %s", field->field_macro, 
			"widget");
	      entry = gtk_object_get_data (GTK_OBJECT (parent), buf);
	    }
	  else
	    {
	      entry = glade_xml_get_widget (xml, field->field_macro);
	    }
	  if (entry)
	    {
	      if (field->field_default)
		set_widget_text (entry, field->field_default);
	      else
		set_widget_text (entry, "");
	    }
	  field_list = g_slist_next (field_list);
	}
    }
}

static void
close_button_cb (GtkWidget *widget, gpointer data)
{
  gtk_widget_destroy (GTK_WIDGET (data));
}

static void
_connect (GladeXML *gui, const char *widget_name, const char *signal_name,
	  GtkSignalFunc callback, gpointer closure)
{
  gtk_signal_connect (
		      GTK_OBJECT (glade_xml_get_widget (gui, widget_name)),
		      signal_name,
		      callback, closure);
}
#define connect(a,b,c,d,e) _connect(a,b,c, GTK_SIGNAL_FUNC(d),e)

void
open_form_panel (forms_def *form)
{
  int          i;
  gchar        buf[1024];
  GSList       *field_list;
  field_def    *foo;
  GtkWidget    *window;
  GtkTooltips  *tips;
  GtkWidget    *label;
  GtkWidget    *e_box;
  GtkWidget    *table;
  GtkWidget    *vbox;
  GtkWidget    *b_box;
  GtkWidget    *frame;
  GtkWidget    *button;
  GtkWidget    *entry;

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window),"Form Generator");
  gtk_window_set_policy (GTK_WINDOW (window), TRUE, TRUE, FALSE);
  gtk_container_border_width (GTK_CONTAINER (window), 2);
  form_dialog_add (window, "Complete Form", window, "Complete Form");
  gtk_object_set_data (GTK_OBJECT (window), "field_count", 
		       GINT_TO_POINTER(form->field_count));
  gtk_object_set_data (GTK_OBJECT (window), "forms_def", form);
  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		      GTK_SIGNAL_FUNC (gtk_main_quit), window);
  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (window), vbox);
  tips = gtk_tooltips_new ();
  table = gtk_table_new (form->field_count, 3, FALSE);
  gtk_box_pack_start (GTK_BOX (vbox), table, FALSE, FALSE, 0);
  field_list = form->field_list;
  i = 0;
  while (field_list)
    {
      foo = (field_def *)field_list->data;
      if (foo)
	{
	  label = gtk_label_new (foo->field_name);
	  gtk_misc_set_alignment (GTK_MISC (label), 0.1, 0.5);
	  e_box = gtk_event_box_new ();
	  gtk_container_add (GTK_CONTAINER (e_box), label);
	  gtk_table_attach (GTK_TABLE (table), e_box, 
			    0, 1, i, i+1,
			    GTK_FILL | GTK_EXPAND, GTK_FILL, 2, 1);
	  entry = gtk_entry_new_with_max_length (foo->field_width);
	  if (foo->field_default)
	    gtk_entry_set_text (GTK_ENTRY (entry), foo->field_default);
	  gtk_table_attach (GTK_TABLE (table), entry,
			    1, 2, i, i+1,
			    GTK_FILL | GTK_EXPAND, GTK_FILL, 2, 1);
	  gtk_object_set_data (GTK_OBJECT (entry), 
			       "field_macro", foo->field_macro);
	  snprintf (buf, sizeof (buf), "%s %s", foo->field_macro, "widget");
	  
	  gtk_object_set_data (GTK_OBJECT (window),
			       buf, entry);
	  if (foo->field_tip)
	    gtk_tooltips_set_tip (tips, e_box, foo->field_tip, NULL);
	}
      else
	{
	  g_print ("No fields?\n");
	}
      field_list = field_list->next;
      i++;
    }
  label = gtk_hseparator_new ();
  gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
  frame = gtk_frame_new (NULL);
  gtk_container_border_width (GTK_CONTAINER (frame), 2);
  gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 0);
  b_box = gtk_hbutton_box_new ();
  gtk_container_border_width (GTK_CONTAINER (b_box), 4);
  gtk_container_add (GTK_CONTAINER (frame), b_box);

  button = gtk_button_new_with_label ("Print...");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (print_button_cb),
		      window);
  gtk_container_add (GTK_CONTAINER (b_box), button);

  button = gtk_button_new_with_label ("Clear");
  gtk_signal_connect (GTK_OBJECT (button), "clicked", 
		      GTK_SIGNAL_FUNC (clear_button_cb),
		      window);
  gtk_container_add (GTK_CONTAINER (b_box), button);

  button = gtk_button_new_with_label ("Close");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (close_button_cb),
		      window);
  gtk_container_add (GTK_CONTAINER (b_box), button);
  gtk_tooltips_enable (tips);
  gtk_widget_show_all (window);
}

void 
open_xml_form_panel (forms_def *form)
{
  GladeXML   *xml;
  GtkWidget  *window;
  GtkWidget  *entry;
  GSList     *field_list;
  field_def  *field;

  gtk_widget_show (entry_window_new (form));

  xml = glade_xml_new (form->xml, "data_entry");
  if (!xml)
    {
      g_error ("Failed to load interface!\n");
      return;
    }
  window = gnome_app_new ("form_maker", _("Complete Form"));
  gtk_container_set_border_width (GTK_CONTAINER (window), 4);
  form_menu_create (window);
  gnome_app_set_contents (window, glade_xml_get_widget (xml, "data_entry"));
  gtk_object_set_data (GTK_OBJECT (window), "xml", xml);
  form_dialog_add (window, "Complete Form", window, form->name);
  gtk_object_set_data (GTK_OBJECT (window), "forms_def", form);
  gtk_object_set_data (GTK_OBJECT (window), "field_count", 
		       GINT_TO_POINTER (form->field_count));
  gtk_object_set_data (GTK_OBJECT (window), "is_xml_form", 
		       GINT_TO_POINTER (TRUE));
  field_list = form->field_list;
  while (field_list)
    {
      field = (field_def *)field_list->data;
      entry = glade_xml_get_widget (xml, field->field_macro);
      if (entry)
	{
	  if (field->field_default)
	    {
	      set_widget_text (entry, field->field_default);
	    }
	}
      field_list = g_slist_next (field_list);
    }
  connect (xml, "close_button", "clicked", close_button_cb, window);
  connect (xml, "clear_button", "clicked", clear_button_cb, window);
  connect (xml, "print_button", "clicked", print_button_cb, window);
}

/* EOF */
