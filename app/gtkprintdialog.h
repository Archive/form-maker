/* -*- Mode: C tab-width: 4 -*-
 * Copyright 1998, 1999 Gregory McLean
 * Author: Gregory McLean <gregm@comstar.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * A quick clone of the netscape motif print dialog.
 */
#ifndef __GTKPRINTDIALOG_H__
#define __GTKPRINTDIALOG_H__

#include <gdk/gdk.h>
#include <gtk/gtkwindow.h>
#include <gtk/gtkwidget.h>
#include <glib.h>

#ifdef __cplusplus
extern "C" { 
#endif /* __cplusplus */
typedef enum
{
  GTK_PRINT_PRINTER,
  GTK_PRINT_FILE
} GtkPrintDestination;
typedef enum
{
  GTK_PRINT_COLOR,
  GTK_PRINT_GREYSCALE
} GtkPrintMethod;
typedef enum
{
  GTK_PRINT_LANDSCAPE,
  GTK_PRINT_PORTRAIT
} GtkPrintOrientation;
typedef enum
{
  GTK_PRINT_FORWARD,
  GTK_PRINT_REVERSE
} GtkPrintDirection;

/****************************************************************************
 * Standard widget macros
 ***************************************************************************/
#define GTK_TYPE_PRINT_SELECTION             (gtk_print_selection_get_type ())
#define GTK_PRINT_SELECTION(obj)             (GTK_CHECK_CAST ((obj), GTK_TYPE_PRINT_SELECTION, GtkPrintSelection))
#define GTK_PRINT_SELECTION_CLASS(klass)     (GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_PRINT_SELECTION, GtkPrintSelectionClass))
#define GTK_IS_PRINT_SELECTION(obj)          (GTK_CHECK_TYPE ((obj), GTK_TYPE_PRINT_SELECTION))
#define GTK_IS_PRINT_SELECTION_CLASS(klass)  (GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_FILE_SELECTION))
/****************************************************************************
 * Control blocks
 ***************************************************************************/
typedef struct _GtkPrintSelection       GtkPrintSelection;
typedef struct _GtkPrintSelectionClass  GtkPrintSelectionClass;
typedef struct _GtkPrinterDef           GtkPrinterDef;
struct _GtkPrintSelection
{
  GtkWindow   window;

  GtkWidget   *command_entry;
  GtkWidget   *filename_entry;
  GtkWidget   *browse_button;
  GtkWidget   *filesel;                /* The file selector for browsing */

  GSList      *destination_group;
  GSList      *paper_size_group;
  GSList      *method_group;
  GSList      *orientation_group;
  GSList      *direction_group;
  
  GtkWidget   *action_box;
  GtkWidget   *print_button;
  GtkWidget   *cancel_button;
  /*
   * Dialog state.
   */
  gint        print_destination;
  gint        print_direction;
  gint        print_method;
  gint        print_orientation;
  gboolean    do_grab;

};
struct _GtkPrintSelectionClass
{
  GtkWindowClass    parent_class;
};

struct _GtkPrinterDef
{
  gint    printer_type;
  gchar   *output_filename;
  gchar   *output_command;

  gint    print_destination;
  gint    print_direction;
  gint    print_method;
  gint    print_orientation;
};

/****************************************************************************
 * Public API
 ***************************************************************************/
GtkType       gtk_print_selection_get_type       (void);
GtkWidget     *gtk_print_selection_new           (const gchar       *title);
void          gtk_print_selection_set_command    (GtkPrintSelection *printsel,
						  const gchar       *command);
void          gtk_print_selection_set_filename   (GtkPrintSelection *printsel,
						  const gchar       *filename);
void          gtk_print_selection_set_definition (GtkPrintSelection *sel,
						  GtkPrinterDef     *def);
void          gtk_print_selection_set_destination (GtkPrintSelection *sel,
						   GtkPrintDestination dest);
void          gtk_print_selection_set_method     (GtkPrintSelection  *sel,
						  GtkPrintMethod     method);
void          gtk_print_selection_set_orientation (GtkPrintSelection *sel,
						   GtkPrintOrientation ori);
void          gtk_print_selection_set_direction  (GtkPrintSelection  *sel,
						  GtkPrintDirection  dirct);

GtkPrinterDef *gtk_print_selection_get_definition (GtkPrintSelection *sel);
#ifdef __cplusplus
}
#endif

#endif /* __GTKPRINTDIALOG_H__ */
