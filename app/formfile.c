/* -*- Mode: C -*-
 * Copyright 1999 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * File handling routines for the form parser.
 * 
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <glib.h>

#include "worksheet.h"
#include "xml-io.h"
#include "formfile.h"

#include "debug.h"

static GList  *avail_forms = NULL;

/****************************************************************************
 * Call this to scan all the files in the template-defs dir and parse them.
 *
 *
 * Function    : parse_forms
 * Description : This function will parse all the form definitions in 
 *               a given directory that is passed as the argument to it.
 *               Returning success or failure.
 * Arguments   : dir   -- The full pathname to the template definition dir.
 * Returns     : TRUE  -- We found at least one form definition in this dir.
 *               FALSE -- None found.
 ***************************************************************************/
gboolean
parse_forms (const char *dir)
{
   struct dirent   **namelist;
   forms_def       *form;
   struct stat     fs;
   char            path[256];
   int             n, i;
   gchar           *buf;
   gboolean        got_one_form = FALSE;

   D_FUNC_START;
   d_print (DEBUG_DUMP, "Scanning dir (%s)\n", dir);

   n = scandir (dir, &namelist, 0, alphasort);
   
   if (n < 0)
     {
       g_warning ("No templates found?\n");
       return FALSE;
     }
   i = 0;
   while (i < n)
     {
       snprintf (path, sizeof (path), "%s/%s", dir, namelist[i]->d_name);
       stat (path, &fs);
       if (S_ISREG(fs.st_mode))
	 {
           if ( (form = ReadXmlForm (path)))
	    {
	      avail_forms = g_list_append (avail_forms, form);
	      form->cfg_path = g_strdup (path);
	      d_print (DEBUG_DUMP, "%s, is a form %s\n", path, form->name);
            }
	 }
       i++;
     }
   if ( (g_list_length (avail_forms)) >= 1)
     {
       D_FUNC_END;
       return TRUE;
     }
   else
     {
       D_FUNC_END;
       return FALSE;
     }
}

/****************************************************************************
 * Function    : parse_a_form
 * Description : This function will parse _one_ form definition (used for
 *               the non-interactive mode) returning success or failure.
 * Arguments   : filename   -- The full path to the form definition to 
 *                             parse.
 * Returns     : TRUE       -- Yep its a good form.
 *               FALSE      -- EEK! Bad form definition.
 ***************************************************************************/
gboolean 
parse_a_form (const char *filename)
{
  forms_def       *form;
  struct stat     fs;

  stat (filename, &fs);
  if (S_ISREG(fs.st_mode))
    {
      if ( (form = ReadXmlForm (filename)) )
	{
	  avail_forms = g_list_append (avail_forms, form);
	  form->cfg_path = g_strdup (filename);
	  return TRUE;
	}
      else
	return FALSE;
    }
  else
    return FALSE;
}

/****************************************************************************
 * avaliable_forms ()
 *
 * Returns the list of available forms.
 ***************************************************************************/
GList *
available_forms ()
{
  return avail_forms;
}
      
/* EOF */





