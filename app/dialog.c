/* -*- Mode: C -*-
 * Copyright 1998, 1999 Gregory McLean
 * Author: Gregory McLean <gregm@comstar.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Misc dialog functions
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <gnome.h>
#include <glade/glade.h>

#include "worksheet.h"
#include "gtkutil.h"
#include "dialog.h"
#include "output.h"
#include "entry_window.h"
#include "gtkprintdialog.h"

#include "debug.h"

static char * unknown_xpm[] = {
"48 48 8 1",
"       s None  c none",
".      c black",
"X      c yellow",
"o      c deeppink",
"O      c midnightblue",
"+      c deepskyblue3",
"@      c firebrick",
"#      c tan",
"                                                ",
"                                                ",
"                 ........                       ",
"              ....XXXXXX...                     ",
"             ..XXXX..........                   ",
"            ..XX...............       ooo       ",
"           ..XX.................     o   o      ",
"          ..XX...................        o      ",
"    .    ..XX......       ........      o       ",
"   ...  ..XX.....           .......    o        ",
"    .   .XX.....             ......    o        ",
"        .X.....               ......            ",
"   ...  .X.....     OO        ......   o        ",
"   ...  ..X...     OOOO        ......           ",
"   ...   ....     OO  O        ......           ",
"   ...               OO        ......           ",
"  ...               OO        .......           ",
" ...                OO       ........           ",
"...                         ..X.....       +++  ",
"..                  OO     ..XX.....      +   + ",
"..      ..          OO    ..XX.....           + ",
"..      ..               ..XX......          +  ",
"...     ..              ..XX......          +   ",
"...    ...            ...XX......           +   ",
" ........            ..XXX......                ",
"  ......            ..XX.......             +   ",
"    ...            ..XX.......     @            ",
"                   .XX......      @@@           ",
"                  ..X.....         @            ",
"                  .XX....                       ",
"                  .X....                        ",
"         ###      .X....          @@@           ",
"        #   #     .X....          @@@           ",
"            #     .X....          @@@           ",
"           #      .X....          @@@           ",
"          #       .X....         @@@            ",
"    ...   #       ......        @@@             ",
"   .   .           ....        @@@              ",
"       .  #                    @@               ",
"      .                        @@      @@       ",
"     .             ....        @@      @@       ",
"     .            .XX...       @@@     @@       ",
"                  .X....       @@@    @@@       ",
"     .            .X....        @@@@@@@@        ",
"                  ......         @@@@@@         ",
"                   ....            @@@          ",
"                                                ",
"                                                "};

static void          print_button_cb                 (GtkWidget   *widget,
						      gpointer    data);
static void          browse_button_cb                (GtkWidget   *widget,
						      gpointer    data);
static void          confirm_ok_button_cb            (GtkWidget   *widget,
						      gpointer    data);
static void          confirm_cancel_button_cb        (GtkWidget   *widget,
						      gpointer    data);
static void          notice_dialog_ok_cb             (GtkWidget   *widget,
						      gpointer    data);

static paper_table size_table[] = {
   { "letter", 612, 792 },      /* 8.5in * 11in */
   { "legal", 612, 1008 },      /* 8.5in * 14in */
   { "ledger", 1224, 792 },     /* 17in * 11in */
   { "tabloid", 792, 1224 },    /* 11in * 17in */
   { "statement", 396, 612 },   /* 5.5in * 8.5in */
   { "executive", 540, 720 },   /* 7.6in * 10in */
   { "folio", 612, 936 },       /* 8.5in * 13in */
   { "quarto", 610, 780 },      /* 8.5in * 10.83in */
   { "10x14", 720, 1008 },      /* 10in * 14in */
   { "A4", 595, 842 },          /* 21cm * 29.7cm */
};


GtkWidget*
create_print_window (GtkWidget *widget)
{
  GtkWidget *print_window;

  print_window = gtk_print_selection_new ("Print:");
  gtk_object_set_data (GTK_OBJECT (print_window), "invoker", widget);
  gtk_print_selection_set_filename (GTK_PRINT_SELECTION (print_window), 
				    "/tmp/output.ps");
  gtk_signal_connect (GTK_OBJECT (GTK_PRINT_SELECTION (print_window)->print_button), "clicked", (GtkSignalFunc) print_button_cb, print_window);
  gtk_print_selection_set_command (GTK_PRINT_SELECTION (print_window), "lpr");
  gtk_print_selection_set_destination (GTK_PRINT_SELECTION (print_window),
				       GTK_PRINT_FILE);
  gtk_grab_add (print_window);
  return print_window;
}


GtkWidget *
printing_dialog_open ()
{
  GtkWidget *printing_progress;
  GtkWidget *window_frame;
  GtkWidget *table1;
  GtkWidget *pbar;
  GtkWidget *label;
  GtkWidget *frame2;
  GtkWidget *hbuttonbox1;
  GtkWidget *button1;

  printing_progress = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_container_border_width(GTK_CONTAINER(printing_progress), 2);
  gtk_window_set_title(GTK_WINDOW(printing_progress), "Printing");

  window_frame = gtk_frame_new(NULL);
  gtk_container_add(GTK_CONTAINER(printing_progress), window_frame);
  gtk_container_border_width(GTK_CONTAINER(window_frame), 2);

  table1 = gtk_table_new(3, 3, FALSE);
  gtk_container_add(GTK_CONTAINER(window_frame), table1);
  gtk_container_border_width(GTK_CONTAINER(table1), 3);

  pbar = gtk_progress_bar_new();
  gtk_object_set_data(GTK_OBJECT(printing_progress), "pbar", pbar);
  gtk_table_attach(GTK_TABLE(table1), pbar, 0, 3, 1, 2,
                   GTK_FILL, GTK_FILL, 2, 3);

  label = gtk_label_new("Printing");
  gtk_object_set_data(GTK_OBJECT(printing_progress), "label", label);
  gtk_table_attach(GTK_TABLE(table1), label, 0, 3, 0, 1,
                   GTK_FILL, GTK_EXPAND | GTK_FILL, 2, 3);

  frame2 = gtk_frame_new(NULL);
  gtk_table_attach(GTK_TABLE(table1), frame2, 0, 3, 2, 3,
                   GTK_FILL, GTK_FILL, 2, 2);
  gtk_container_border_width(GTK_CONTAINER(frame2), 2);

  hbuttonbox1 = gtk_hbutton_box_new();
  gtk_container_add(GTK_CONTAINER(frame2), hbuttonbox1);
  gtk_container_border_width(GTK_CONTAINER(hbuttonbox1), 3);

  button1 = gtk_button_new_with_label("Cancel");
  gtk_object_set_data(GTK_OBJECT(printing_progress), "button1", button1);
  gtk_container_add(GTK_CONTAINER(hbuttonbox1), button1);

  gtk_widget_show_all (window_frame);
  gtk_widget_show_now (printing_progress);

  return printing_progress;

}

void
printing_dialog_close (GtkWidget *widget)
{
  g_return_if_fail (widget != NULL);

  gtk_widget_destroy (widget);
}

void 
printing_dialog_update (GtkWidget *widget, gfloat percent)
{
  GtkWidget  *pbar;

  g_return_if_fail (widget != NULL);

  pbar = get_widget (widget, "pbar");
  if (GTK_IS_WIDGET (pbar))
    gtk_progress_bar_update (GTK_PROGRESS_BAR (pbar), percent);
  gtk_main_iteration ();
}

/****************************************************************************
 * set_widget_text  -- Simple function that will set the text in a widget
 *                     to a said value.
 ***************************************************************************/
void
set_widget_text (GtkWidget *widget, gchar *text)
{
  GList       *children = NULL;
  GtkWidget   *label;

  g_return_if_fail (GTK_IS_WIDGET (widget));

  if (GTK_IS_ENTRY (widget))
    {
      gtk_entry_set_text (GTK_ENTRY (widget), text);
      return;
    }
  if (GTK_IS_OPTION_MENU (widget))
    {
      /* FIXME: Write me!! */
    }
}
/****************************************************************************
 * get_widget_text  -- Simple function that will get the textual 
 *                     representation of a widget.
 ***************************************************************************/
gchar *
get_widget_text (GtkWidget *widget)
{
  GList        *children = NULL;
  GtkWidget    *label;
  gchar        *text; 

  g_return_val_if_fail (GTK_IS_WIDGET (widget), NULL);

  if (GTK_IS_ENTRY(widget))
      return g_strdup (gtk_entry_get_text (GTK_ENTRY (widget)));
  if (GTK_IS_OPTION_MENU (widget))
    {
      children = gtk_container_children (GTK_CONTAINER (widget));
      label = children->data;
      if (GTK_IS_ACCEL_LABEL (label))
	  gtk_label_get (GTK_LABEL (label), &text);
      return g_strdup (text);
    }
  if (GTK_IS_COMBO(widget))
    {
      g_print ("Combo widget\n");
      return "";
    }
  
  return NULL;
}

static void
print_button_cb (GtkWidget *widget, gpointer data)
{
  int            i;
  GSList         *field_datum = NULL;
  GSList         *field_list;
  field_def      *field;
  GtkWidget      *entry;
  GtkWidget      *invoker;
  wks_data       *datum;
  forms_def      *form;
  GtkPrinterDef  *printer;

  gboolean   xml_form;
  GladeXML   *xml;
  char       buf [1024];

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_WIDGET (data));
  
  if (GTK_WIDGET_HAS_GRAB (data))
    gtk_grab_remove (GTK_WIDGET (data));
  gtk_widget_hide (data);
  invoker = get_widget (GTK_WIDGET (data), "invoker");
  form = ENTRY_WINDOW (invoker)->form; 
  field_list = form->field_list;
  xml = ENTRY_WINDOW (invoker)->xml;
  while (field_list)
    {
      field = (field_def *)field_list->data;
      datum = g_new0 (wks_data, 1);
      datum->field_name = g_strdup (field->field_macro);
      entry = glade_xml_get_widget (xml, field->field_macro);
      if (entry)
	datum->field_value = get_widget_text (entry);
      else
	datum->field_value = g_strdup ("");
      field_datum = g_slist_append (field_datum, datum);
      field_list = field_list->next;
    }
  printer = gtk_print_selection_get_definition (GTK_PRINT_SELECTION (data));

  makeworksheet (field_datum, form, printer, TRUE); 
  gtk_widget_destroy (data);
}

static void
browse_button_cb (GtkWidget *widget, gpointer data)
{
  GtkWidget *selector;
  
  gtk_grab_remove (data);
  selector = open_fileselection (data, TRUE);

}

/*
 * File selection dialog
 */
static int           file_dialog_hide          (GtkWidget    *filesel);

GtkWidget *
open_fileselection (GtkWidget *parent, gboolean parent_grab)
{
  GtkWidget   *fileselection;
  gchar       *filename;

  g_return_if_fail (parent != NULL);

  fileselection = (GtkWidget *)g_malloc (sizeof (GtkWidget));
  
  fileselection = gtk_file_selection_new ("Select File");
  gtk_container_border_width (GTK_CONTAINER (fileselection), 10);
  gtk_object_set_data (GTK_OBJECT (fileselection), "invoker_widget",
		       (GtkWidget *)parent);
  gtk_object_set_data (GTK_OBJECT (fileselection), "parent_grab", 
		       GINT_TO_POINTER (parent_grab));
  filename = gtk_object_get_data (GTK_OBJECT (parent), "file_name");
  gtk_file_selection_set_filename (GTK_FILE_SELECTION (fileselection), 
				   (filename) ? filename : "");
  gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (fileselection));
  gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION 
					 (fileselection)->cancel_button),
			     "clicked",
			     (GtkSignalFunc) file_dialog_hide,
			     GTK_OBJECT (fileselection));
  gtk_signal_connect (GTK_OBJECT (fileselection),
		      "delete_event",
		      (GtkSignalFunc) file_dialog_hide,
		      NULL);

  gtk_widget_show_all (fileselection);
  return (GtkWidget *)fileselection;
}

void
close_fileselection (GtkWidget *widget)
{
  gtk_widget_destroy (widget);
  if (widget)
    g_free (widget);
}

static int
file_dialog_hide (GtkWidget *filesel)
{
  GtkWidget    *widget;

  widget = get_widget (filesel, "invoker_widget");
  if (widget)
    if ( GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (filesel), 
					       "parent_grab")) )
      gtk_grab_add (widget);
  gtk_widget_destroy (filesel);
  return TRUE;
}

gboolean
confirmation_dialog (char *msg, ...)
{
  va_list ap;
  gchar   buf[1024];
  gboolean   retval;
  GtkWidget *confirm_dialog;
  GtkWidget *frame;
  GtkWidget *table;
  GdkPixmap *unkn_pixmap;
  GdkBitmap *unkn_mask;
  GtkWidget *pixmap;
  GtkWidget *hseparator1;
  GtkWidget *label;
  GtkWidget *hbuttonbox;
  GtkWidget *button;

  va_start (ap, msg);
  vsnprintf (buf, sizeof(buf), msg, ap);
  va_end (ap);

  confirm_dialog = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_container_border_width(GTK_CONTAINER(confirm_dialog), 4);
  gtk_window_set_title(GTK_WINDOW(confirm_dialog), "Confirm?");
  frame = gtk_frame_new(NULL);
  gtk_container_add(GTK_CONTAINER(confirm_dialog), frame);

  table = gtk_table_new(3, 3, FALSE);
  gtk_container_add(GTK_CONTAINER(frame), table);
  gtk_container_border_width(GTK_CONTAINER(table), 4);

  gtk_widget_realize(confirm_dialog);
  unkn_pixmap = gdk_pixmap_create_from_xpm_d (confirm_dialog->window,
					       &unkn_mask,
					       &confirm_dialog->style->bg[GTK_STATE_NORMAL],
					       unknown_xpm);
  pixmap = gtk_pixmap_new (unkn_pixmap, unkn_mask);
  gtk_table_attach(GTK_TABLE(table), pixmap, 0, 1, 0, 1,
                   GTK_FILL, GTK_FILL, 0, 0);
  gtk_misc_set_padding(GTK_MISC(pixmap), 10, 10);

  hseparator1 = gtk_hseparator_new();
  gtk_table_attach(GTK_TABLE(table), hseparator1, 0, 3, 1, 2,
                   GTK_FILL, GTK_FILL, 3, 4);

  label = gtk_label_new(buf);
  gtk_table_attach(GTK_TABLE(table), label, 1, 3, 0, 1,
                   GTK_FILL, GTK_FILL, 0, 0);

  hbuttonbox = gtk_hbutton_box_new();
  gtk_table_attach(GTK_TABLE(table), hbuttonbox, 0, 3, 2, 3,
                   GTK_FILL, GTK_FILL, 0, 0);
  gtk_container_border_width(GTK_CONTAINER(hbuttonbox), 4);
  
  button = gtk_button_new_with_label("Ok");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc)confirm_ok_button_cb,
		      confirm_dialog);
  gtk_container_add(GTK_CONTAINER(hbuttonbox), button);
  GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);

  button = gtk_button_new_with_label("Cancel");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc)confirm_cancel_button_cb,
		      confirm_dialog);
  gtk_container_add(GTK_CONTAINER(hbuttonbox), button);
  GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
  gtk_widget_show_all (confirm_dialog);

  gtk_main ();
  retval = GPOINTER_TO_INT(gtk_object_get_data (GTK_OBJECT (confirm_dialog), 
						"return_value"));
  gtk_widget_destroy (confirm_dialog);
  return retval;
}
 
static void
confirm_ok_button_cb (GtkWidget *widget, gpointer data)
{
  g_return_if_fail (data != NULL);

  gtk_object_set_data (GTK_OBJECT (data), "return_value",
		       GINT_TO_POINTER (TRUE));
  gtk_main_quit();
}

static void
confirm_cancel_button_cb (GtkWidget *widget, gpointer data)
{
  g_return_if_fail (GTK_IS_OBJECT (data));

  gtk_object_set_data (GTK_OBJECT (data), "return_value",
		       (gboolean *)FALSE);
  gtk_main_quit();
}

/*
 * Misc support functions
 */

/*
 * Function   : paper_size
 * Description: returns the paper_table struct for the paper named 'name'
 *              passed in as an argument. Null is returned on failure.
 * Arguments  : name -- the name of the paper to find. This is matched
 *                      case insensitivly.
 * Returns    : A pointer to the paper table entry on success or NULL 
 *              on failure.
 */
paper_table *
paper_size (const char *name)
{
  paper_table    *p_table;

  g_return_val_if_fail (name != NULL, NULL);

  for (p_table = size_table; p_table->paper_name; p_table++)
    {
      if (strcasecmp (p_table->paper_name, name) == 0) 
	{ /* We have a match! */
	  return p_table;
	}
    }
  return (paper_table *)NULL;
}

/*
 * Function   : notice_dialog
 * Description: This function will pop up a dialog that just informs the user
 *              of some problem. If using gnome this will be mapped into the
 *              gnome_message_box stuff else it will build its own dialog.
 * Arguments  : modal -- True or False as to make this dialog modal
 *              msg   -- The message to put in the box.
 *              ...   -- Variable for the message.
 */
static GtkWidget *d_notice_dialog = NULL;
void
notice_dialog (gboolean modal, gchar *msg, ...)
{
  va_list     ap;
  gchar       buf[1024];

  GtkWidget *frame;
  GtkWidget *table;
  GtkWidget *button_frame;
  GtkWidget *button;
  GtkWidget *hseparator;
  GdkPixmap *note_pixmap;
  GdkBitmap *note_mask;
  GtkWidget *pixmap;
  GtkWidget *label;

  va_start (ap, msg);
  vsnprintf (buf, sizeof(buf), msg, ap);
  va_end (ap);

  if (d_notice_dialog == NULL)
    {
      d_notice_dialog = gtk_window_new(GTK_WINDOW_TOPLEVEL);
      gtk_window_set_policy (GTK_WINDOW (d_notice_dialog), TRUE, TRUE, TRUE);
      gtk_container_border_width(GTK_CONTAINER(d_notice_dialog), 4);
      gtk_window_set_title(GTK_WINDOW(d_notice_dialog), "Notice!");
      
      gtk_object_set_data (GTK_OBJECT (d_notice_dialog), "modal", 
			   GINT_TO_POINTER (modal));
      
      frame = gtk_frame_new(NULL);
      gtk_container_add(GTK_CONTAINER(d_notice_dialog), frame);

      table = gtk_table_new(3, 3, FALSE);
      gtk_widget_show(table);
      gtk_container_add(GTK_CONTAINER(frame), table);
      
      button_frame = gtk_frame_new(NULL);
      gtk_table_attach(GTK_TABLE(table), button_frame, 
		       0, 3, 2, 3,
		       GTK_FILL, GTK_FILL, 0, 0);
      gtk_container_border_width(GTK_CONTAINER(button_frame), 2);
      
      button = gtk_button_new_with_label("Ok");
      gtk_signal_connect (GTK_OBJECT (button), "clicked",
			  (GtkSignalFunc) notice_dialog_ok_cb,
			  d_notice_dialog);
      gtk_container_add(GTK_CONTAINER(button_frame), button);
      gtk_container_border_width(GTK_CONTAINER(button), 4);
      
      hseparator = gtk_hseparator_new();
      gtk_table_attach(GTK_TABLE(table), hseparator, 
		       0, 3, 1, 2,
		       GTK_FILL, GTK_FILL, 4, 4);
      
      label = gtk_label_new(buf);
      gtk_object_set_data (GTK_OBJECT (d_notice_dialog), "notice_label",
			   label);
      gtk_misc_set_padding (GTK_MISC (label), 10, 10);
      gtk_table_attach(GTK_TABLE(table), label, 
		       1, 3, 0, 1,
		       GTK_FILL, GTK_FILL, 0, 0);
      
      gtk_widget_show_all (frame);
      gtk_widget_show_now (d_notice_dialog);
    }
  else
    {
      label = get_widget (d_notice_dialog, "notice_label");
      gtk_label_set (GTK_LABEL (label), buf);
      gdk_window_raise ((GdkWindow *)d_notice_dialog->window);
    }
  
}

static void
notice_dialog_ok_cb (GtkWidget *widget, gpointer data)
{
  g_return_if_fail (d_notice_dialog != NULL);

  gtk_widget_destroy (d_notice_dialog);
  d_notice_dialog = NULL;

}
/* EOF */

