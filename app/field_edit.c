/* -*- Mode: C -*-
 * Copyright (C) 1998 Gregory McLean
 * Author: Gregory McLean <gregm@comstar.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <gnome.h>

#include "worksheet.h"
#include "gtkutil.h"
#include "field_edit.h"

#include "debug.h"

/****************************************************************************
 * Table of descriptive labels to display as the first column of the table
 ***************************************************************************/
static char *field_names[] = {
  N_("Field Name"),
  N_("Field Macro Name"),
  N_("Default Value"),
  N_("Field Tooltip"),
  N_("Field Type"),
  N_("Field Maximum Size")
};
/****************************************************************************
 * Table of tooltips to associate with the table columns
 ***************************************************************************/
static char *field_tooltip[] = {
  "The label that should be displayed for this field on the entry forms.",
  "The PostScript macro that should be output for this field in the "
  "resulting field.",
  "This should be set to the default value for this field, if any.",
  "Set this to the tooltip for this field on the data entry forms.",
  "The type of field that this is.",
  "The maximum size of the field, when appropriate."
};
/****************************************************************************
 * Signal information
 ***************************************************************************/
enum {
  CHANGED_SIGNAL,
  LAST_SIGNAL
};
static guint signals[LAST_SIGNAL] = { 0 };
/****************************************************************************
 * Local forward references
 ***************************************************************************/
static void    field_edit_class_init        (field_edit_panelClass   *klass);
static void    field_edit_init              (field_edit_panel        *panel);
static void    changed_cb                   (GtkWidget               *widget,
					     gpointer                data);
/****************************************************************************
 * field_edit_panel_get_type ()
 ***************************************************************************/
GtkType
field_edit_get_type ()
{
  static GtkType ed_type = 0;
  if (!ed_type)
    {
     GtkTypeInfo ed_info = 
     {
       "field_edit_panel",
       sizeof (field_edit_panel),
       sizeof (field_edit_panelClass),
       (GtkClassInitFunc) field_edit_class_init,
       (GtkObjectInitFunc) field_edit_init,
       /* reserved */ NULL,
       /* reserved */ NULL,
       (GtkClassInitFunc) NULL
     };
     ed_type = gtk_type_unique (gtk_vbox_get_type (), &ed_info);
    }
  return ed_type;
}
/****************************************************************************
 * Class initialization.
 ***************************************************************************/
static void
field_edit_class_init (field_edit_panelClass *klass)
{
  GtkObjectClass  *object_class;
  object_class = (GtkObjectClass *)klass;
  signals[CHANGED_SIGNAL] = 
    gtk_signal_new ("changed",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (field_edit_panelClass, changed),
		    gtk_signal_default_marshaller,
		    GTK_TYPE_NONE, 0);
  gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);
  klass->changed = NULL;
}
/****************************************************************************
 * Widget initialization 
 ***************************************************************************/
static void
field_edit_init (field_edit_panel *panel)
{
  GtkWidget    *table;
  GtkTooltips  *tooltips;
  GtkWidget    *label;
  GtkWidget    *event_box;
  gint         i;
  
  D_FUNC_START;
  table = gtk_table_new (ELEMENTS (field_names), 2, FALSE);
  gtk_table_set_row_spacings (GTK_TABLE (table), 4);
  gtk_table_set_col_spacings (GTK_TABLE (table), 4);
  gtk_container_border_width (GTK_CONTAINER (table), 2);
  gtk_container_add (GTK_CONTAINER (panel), table);

  /* 
   * Lets build the label widgets adding the tooltips.
   */
  DPRT ("Tooltip/label setup");
  tooltips = gtk_tooltips_new ();
  for ( i = 0; i < ELEMENTS (field_names); i++ )
    {
      label = gtk_label_new (field_names[i]);
      gtk_misc_set_alignment (GTK_MISC (label), 0.1, 0.5);
      event_box = gtk_event_box_new ();
      gtk_container_add (GTK_CONTAINER (event_box), label);
      gtk_tooltips_set_tip (tooltips, event_box, field_tooltip[i], NULL);
      gtk_table_attach (GTK_TABLE (table), event_box, 0, 1, i, i+1,
			GTK_FILL, 0, 0, 0);
    }
  DPRT ("Tooltips/labels set up.");
  /* 
   * Now for the actual entry widgets.
   *
   * First the field name
   */
  panel->field_macro = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT (panel->field_macro), "changed",
		      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->field_macro, 1, 2, 0, 1,
		    GTK_FILL, 0, 0, 0);
  /* 
   * The field macro
   */
  panel->field_macro = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT (panel->field_macro), "changed",
		      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->field_macro, 1, 2, 1, 2,
		    GTK_FILL, 0, 0, 0);
  /*
   * The default value for this field.
   */
  panel->field_default = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT (panel->field_default), "changed",
		      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->field_default, 1, 2, 2, 3,
		    GTK_FILL, 0, 0, 0);
  /*
   * The tooltip for this field.
   */
  panel->field_tooltip = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT (panel->field_tooltip), "changed",
		      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->field_tooltip, 1, 2, 3, 4,
		    GTK_FILL, 0, 0, 0);
  /*
   * The max size for this widget
   */
  panel->field_max_size = gtk_entry_new ();
  gtk_signal_connect (GTK_OBJECT (panel->field_max_size), "changed",
		      (GtkSignalFunc) changed_cb, panel);
  gtk_table_attach (GTK_TABLE (table), panel->field_max_size, 1, 2, 5, 6,
		    GTK_FILL, 0, 0, 0);
  gtk_widget_show_all (table);
  D_FUNC_END;
  
}
/****************************************************************************
 * Changed function standard handler.
 ***************************************************************************/
static void
changed_cb (GtkWidget *widget, gpointer data)
{
  field_edit_panel    *panel;

  panel = FIELD_EDIT (data);
  gtk_signal_emit (GTK_OBJECT (panel), signals[CHANGED_SIGNAL]);
}
/****************************************************************************
 * Public API
 *
 * new() method
 ***************************************************************************/
GtkWidget *
field_edit_new ()
{
  return GTK_WIDGET (gtk_type_new (field_edit_get_type ()) );
}
/****************************************************************************
 * put() method
 ***************************************************************************/
void
field_edit_put_data (field_edit_panel *panel, field_def *field)
{
  gtk_entry_set_text (GTK_ENTRY (panel->field_macro), 
		      field->field_name ? field->field_name : "");
  gtk_entry_set_text (GTK_ENTRY (panel->field_macro),
		      field->field_macro ? field->field_macro : "");
  gtk_entry_set_text (GTK_ENTRY (panel->field_default),
		      field->field_default ? field->field_default : "");
  gtk_entry_set_text (GTK_ENTRY (panel->field_tooltip),
		      field->field_tip ? field->field_tip : "");
}
/****************************************************************************
 * get() method
 * 
 * Returns TRUE if something in the panel changed, FALSE otherwise.
 ***************************************************************************/
gboolean
field_edit_get_data (field_edit_panel *panel, field_def *field)
{
  gboolean     changed = FALSE;
  gint         i;

  changed |= update_string_field (&field->field_name,
		    gtk_entry_get_text (GTK_ENTRY (panel->field_macro)));
  changed |= update_string_field (&field->field_macro,
		    gtk_entry_get_text (GTK_ENTRY (panel->field_macro)));
  changed |= update_string_field (&field->field_default,
		    gtk_entry_get_text (GTK_ENTRY (panel->field_default)));
  changed |= update_string_field (&field->field_tip,
		    gtk_entry_get_text (GTK_ENTRY (panel->field_tooltip)));

  return (changed != FALSE);
}
/* EOF */

