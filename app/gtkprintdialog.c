/* -*- Mode: C -*-
 * Copyright 1998, 1999 Gregory McLean
 * Author: Gregory McLean <gregm@comstar.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * A quick clone of the netscape motif print dialog.
 */

#include <gtk/gtk.h>
#include <gtk/gtkwindow.h>
#include <gtk/gtkobject.h>
#include <gtk/gtkwidget.h>

#include "gtkprintdialog.h"

/****************************************************************************
 * Forward declaration
 ***************************************************************************/
static void gtk_print_selection_class_init (GtkPrintSelectionClass  *klass);
static void gtk_print_selection_init       (GtkPrintSelection       *printsel);
static void gtk_print_selection_destroy    (GtkObject               *object);
static void gtk_print_cancel_button_cb     (GtkWidget               *widget,
					    GtkPrintSelection       *sel);
static void gtk_print_browse_button_cb     (GtkWidget               *widget,
					    GtkPrintSelection       *sel);
static void gtk_print_destroy_filesel      (GtkWidget               *widget,
					    GtkFileSelection        *filesel);
static void gtk_print_filesel_ok_cb        (GtkWidget               *widget,
					    GtkPrintSelection       *filesel);
static void gtk_print_destination_cb       (GtkWidget               *widget,
					    GtkPrintSelection       *sel);
static void gtk_print_method_cb            (GtkWidget               *widget,
					    GtkPrintSelection       *sel);
static void gtk_print_orientation_cb       (GtkWidget               *widget,
					    GtkPrintSelection       *sel);
static void gtk_print_direction_cb         (GtkWidget               *widget,
					    GtkPrintSelection       *sel);
static void gtk_print_set_radiobutton      (GSList                  *list,
					    gint                    value);
/****************************************************************************
 * Local data
 ***************************************************************************/
static GtkWindowClass *parent_class = NULL;
/****************************************************************************
 * The widget!
 ***************************************************************************/
/** 
 * gtk_print_selection_get_type:
 *
 * Registers the &GtkPrintSelection class if necessary, and returns the type
 * ID associated to it.
 *
 * Return value: The type ID of the &GtkPrintSelection class.
 *
 **/
GtkType 
gtk_print_selection_get_type (void)
{
  static GtkType print_selection_type = 0;
  if (!print_selection_type)
    {
      static const GtkTypeInfo printsel_info = 
      {
	"GtkPrintSelection",
	sizeof (GtkPrintSelection),
	sizeof (GtkPrintSelectionClass),
	(GtkClassInitFunc) gtk_print_selection_class_init,
	(GtkObjectInitFunc) gtk_print_selection_init,
	/* reserved_1 */ NULL,
	/* reserved_2 */ NULL,
	(GtkClassInitFunc) NULL
      };
      print_selection_type = gtk_type_unique (GTK_TYPE_WINDOW, &printsel_info);
    }
  return print_selection_type;
}

static void
gtk_print_selection_class_init (GtkPrintSelectionClass *klass)
{
  GtkObjectClass  *object_class;
  
  object_class = (GtkObjectClass *)klass;
  parent_class = gtk_type_class (GTK_TYPE_WINDOW);
  object_class->destroy = gtk_print_selection_destroy;
}

static void
gtk_print_selection_init (GtkPrintSelection *printsel)
{
  /* Build the widget */
  GtkWidget     *window_table;
  GtkWidget     *paper_table;
  GtkWidget     *label;
  GtkWidget     *radiobutton;
  
  gtk_container_set_border_width (GTK_CONTAINER (printsel), 5);

  window_table = gtk_table_new (10, 4, FALSE);
  gtk_table_set_row_spacings (GTK_TABLE (window_table), 5);
  gtk_container_add (GTK_CONTAINER (printsel), window_table);
  printsel->do_grab = FALSE;
  /***********
   * Print to:
   **********/
  label = gtk_label_new ("Print To:");
  gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
  gtk_table_attach (GTK_TABLE (window_table), label,
		    0, 1, 0, 1,
		    GTK_FILL, 0, 2, 0);
  radiobutton = gtk_radio_button_new_with_label (printsel->destination_group,
						 "Printer");
  gtk_object_set_data (GTK_OBJECT (radiobutton), "value",
		       GINT_TO_POINTER (GTK_PRINT_PRINTER));
  gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (radiobutton), TRUE);
  printsel->destination_group = gtk_radio_button_group (GTK_RADIO_BUTTON (radiobutton));
  gtk_signal_connect (GTK_OBJECT (radiobutton), "clicked",
		      gtk_print_destination_cb,
		      printsel);
  gtk_table_attach (GTK_TABLE (window_table), radiobutton, 1, 2, 0, 1,
		    GTK_FILL, 0, 0, 0);
  
  radiobutton = gtk_radio_button_new_with_label (printsel->destination_group,
						 "File");
  gtk_object_set_data (GTK_OBJECT (radiobutton), "value",
		       GINT_TO_POINTER (GTK_PRINT_FILE));
  printsel->destination_group = gtk_radio_button_group (GTK_RADIO_BUTTON (radiobutton));
  gtk_signal_connect (GTK_OBJECT (radiobutton), "clicked",
		      gtk_print_destination_cb,
		      printsel);
  gtk_table_attach (GTK_TABLE (window_table), radiobutton, 2, 4, 0, 1,
		    GTK_FILL, 0, 0, 0);
  printsel->print_destination = GTK_PRINT_PRINTER;

  /***********
   * Print Command
   **********/
  label = gtk_label_new ("Print Command:");
  gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
  gtk_table_attach (GTK_TABLE (window_table), label,
		    0, 1, 1, 2,
		    GTK_FILL, 0, 2, 0);
  printsel->command_entry = gtk_entry_new ();
  gtk_table_attach (GTK_TABLE (window_table), printsel->command_entry,
		    1, 4, 1, 2, 
		    GTK_FILL, 0, 2, 2);

  /***********
   * Output filename
   **********/
  label = gtk_label_new ("File Name:");
  gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
  gtk_table_attach (GTK_TABLE (window_table), label,
		    0, 1, 2, 3, 
		    GTK_FILL, 0, 2, 0);
  printsel->filename_entry = gtk_entry_new ();
  gtk_table_attach (GTK_TABLE (window_table), printsel->filename_entry,
		    1, 3, 2, 3,
		    GTK_FILL , 0, 2, 2);
  gtk_widget_set_sensitive (printsel->filename_entry, FALSE);
  printsel->browse_button = gtk_button_new_with_label ("Browse...");
  gtk_signal_connect (GTK_OBJECT (printsel->browse_button), "clicked",
		      (GtkSignalFunc) gtk_print_browse_button_cb,
		      printsel);
  gtk_table_attach (GTK_TABLE (window_table), printsel->browse_button,
		    3, 4, 2, 3, 
		    GTK_FILL, 0, 2, 3);
  gtk_widget_set_sensitive (printsel->browse_button, FALSE);
  /***********
   * Print settings area
   ***********/
  label = gtk_hseparator_new ();
  gtk_table_attach (GTK_TABLE (window_table), label, 0, 4, 3, 4,
		    GTK_FILL, 0, 3, 3);
  /* Direction */
  label = gtk_label_new ("Print:");
  gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
  gtk_table_attach (GTK_TABLE (window_table), label,
		    0, 1, 4, 5,
		    GTK_FILL, 0, 2, 0);
  radiobutton = gtk_radio_button_new_with_label (printsel->direction_group,
						"First Page First");
  printsel->direction_group = gtk_radio_button_group (GTK_RADIO_BUTTON (radiobutton));
  gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (radiobutton), TRUE);
  gtk_signal_connect (GTK_OBJECT (radiobutton), "clicked",
		      (GtkSignalFunc) gtk_print_direction_cb,
		      printsel);
  gtk_table_attach (GTK_TABLE (window_table), radiobutton,
		    1, 2, 4, 5,
		    GTK_FILL, 0, 0, 0);

  radiobutton = gtk_radio_button_new_with_label (printsel->direction_group,
						"Last Page First");
  printsel->direction_group = gtk_radio_button_group (GTK_RADIO_BUTTON (radiobutton));
  gtk_signal_connect (GTK_OBJECT (radiobutton), "clicked",
		      (GtkSignalFunc) gtk_print_direction_cb,
		      printsel);
  gtk_table_attach (GTK_TABLE (window_table), radiobutton, 
		    2, 4, 4, 5,
		    GTK_FILL, 0, 0, 0);
  printsel->print_direction = GTK_PRINT_FORWARD;
  /* Orientation */
  label = gtk_label_new ("Orientation:");
  gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
  gtk_table_attach (GTK_TABLE (window_table), label,
		    0, 1, 5, 6,
		    GTK_FILL, 0, 2, 0);
  radiobutton = gtk_radio_button_new_with_label (printsel->orientation_group,
						 "Portrait");
  gtk_object_set_data (GTK_OBJECT (radiobutton), "value", 
		       GINT_TO_POINTER (GTK_PRINT_PORTRAIT));
  printsel->orientation_group = gtk_radio_button_group (GTK_RADIO_BUTTON (radiobutton));
  gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (radiobutton), TRUE);
  gtk_signal_connect (GTK_OBJECT (radiobutton), "clicked",
		      (GtkSignalFunc)gtk_print_orientation_cb,
		      printsel);
  gtk_table_attach (GTK_TABLE (window_table), radiobutton,
		    1, 2, 5, 6,
		    GTK_FILL, 0, 0, 0);
  
  radiobutton = gtk_radio_button_new_with_label (printsel->orientation_group,
						 "Landscape");
  gtk_object_set_data (GTK_OBJECT (radiobutton), "value",
		       GINT_TO_POINTER (GTK_PRINT_LANDSCAPE));
  printsel->orientation_group = gtk_radio_button_group (GTK_RADIO_BUTTON (radiobutton));
  gtk_signal_connect (GTK_OBJECT (radiobutton), "clicked",
		      (GtkSignalFunc)gtk_print_orientation_cb,
		      printsel);
  gtk_table_attach (GTK_TABLE (window_table), radiobutton,
		    2, 4, 5, 6,
		    GTK_FILL, 0, 0, 0);
  printsel->print_orientation = GTK_PRINT_PORTRAIT;

  /* Color/Greyscale */
  label = gtk_label_new ("Print:");
  gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
  gtk_table_attach (GTK_TABLE (window_table), label,
		    0, 1, 6, 7,
		    GTK_FILL, 0, 2, 0);
  radiobutton = gtk_radio_button_new_with_label (printsel->method_group,
						 "Greyscale");
  gtk_object_set_data (GTK_OBJECT (radiobutton), "value",
		       GINT_TO_POINTER (GTK_PRINT_GREYSCALE));
  printsel->method_group = gtk_radio_button_group (GTK_RADIO_BUTTON (radiobutton));
  gtk_signal_connect (GTK_OBJECT (radiobutton), "clicked",
		      (GtkSignalFunc) gtk_print_method_cb,
		      printsel);
  gtk_table_attach (GTK_TABLE (window_table), radiobutton, 
		    1, 2, 6, 7,
		    GTK_FILL, 0, 0, 0);

  radiobutton = gtk_radio_button_new_with_label (printsel->method_group,
						 "Color");
  gtk_object_set_data (GTK_OBJECT (radiobutton), "value",
		       GINT_TO_POINTER (GTK_PRINT_COLOR));
  printsel->method_group = gtk_radio_button_group (GTK_RADIO_BUTTON (radiobutton));
  gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (radiobutton), TRUE);
  gtk_signal_connect (GTK_OBJECT (radiobutton), "clicked",
		      (GtkSignalFunc) gtk_print_method_cb,
		      printsel);
  gtk_table_attach (GTK_TABLE (window_table), radiobutton,
		    2, 4, 6, 7, GTK_FILL, 0, 0, 0);
  printsel->print_method = GTK_PRINT_COLOR;

  /* Paper Size */
  label = gtk_label_new ("Paper Size:");
  gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.2);
  gtk_table_attach (GTK_TABLE (window_table), label,
		    0, 1, 7, 8, 
		    GTK_FILL, GTK_FILL, 2, 0);
  paper_table = gtk_table_new (2, 2, FALSE);
  gtk_table_attach (GTK_TABLE (window_table), paper_table,
		    1, 4, 7, 8,
		    GTK_FILL, GTK_FILL, 2, 2);
  gtk_container_border_width (GTK_CONTAINER (paper_table), 2);

  radiobutton = gtk_radio_button_new_with_label (printsel->paper_size_group,
						 "Letter (8 1/2 x 11 in.)");
  printsel->paper_size_group = gtk_radio_button_group (GTK_RADIO_BUTTON (radiobutton));
  gtk_table_attach (GTK_TABLE (paper_table), radiobutton,
		    0, 1, 0, 1, GTK_FILL, 0, 0, 0);
  
  radiobutton = gtk_radio_button_new_with_label (printsel->paper_size_group,
						 "Executive (7 1/2 x 10 in.)");
  printsel->paper_size_group = gtk_radio_button_group (GTK_RADIO_BUTTON (radiobutton));
  gtk_table_attach (GTK_TABLE (paper_table), radiobutton,
		    0, 1, 1, 2, GTK_FILL, 0, 0, 0);

  radiobutton = gtk_radio_button_new_with_label (printsel->paper_size_group,
						 "Legal (8 1/2 x 14 in.)");
  printsel->paper_size_group = gtk_radio_button_group (GTK_RADIO_BUTTON (radiobutton));
  gtk_table_attach (GTK_TABLE (paper_table), radiobutton,
		    1, 2, 0, 1, GTK_FILL, 0, 0, 0);

  radiobutton = gtk_radio_button_new_with_label (printsel->paper_size_group,
						 "A4 (210 x 297 mm)");
  printsel->paper_size_group = gtk_radio_button_group (GTK_RADIO_BUTTON (radiobutton));
  gtk_table_attach (GTK_TABLE (paper_table), radiobutton,
		    1, 2, 1, 2, GTK_FILL, 0, 0, 0);

  /***********
   * Action box
   **********/
  label = gtk_hseparator_new ();
  gtk_table_attach (GTK_TABLE (window_table), label, 0, 4, 8, 9, 
		    GTK_FILL , 0, 3, 4);
  printsel->action_box = gtk_hbutton_box_new ();
  gtk_container_border_width (GTK_CONTAINER (printsel->action_box), 3);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (printsel->action_box),
			     GTK_BUTTONBOX_EDGE);
  gtk_table_attach (GTK_TABLE (window_table), printsel->action_box,
		    0, 4, 9, 10, 
		    GTK_FILL, 0, 3, 1);

  printsel->print_button = gtk_button_new_with_label ("Print");
  GTK_WIDGET_SET_FLAGS (printsel->print_button, GTK_CAN_DEFAULT);
  gtk_container_add (GTK_CONTAINER (printsel->action_box), 
		     printsel->print_button);
  
  printsel->cancel_button = gtk_button_new_with_label ("Cancel");
  GTK_WIDGET_SET_FLAGS (printsel->cancel_button, GTK_CAN_DEFAULT);
  gtk_signal_connect (GTK_OBJECT (printsel->cancel_button), "clicked",
		      (GtkSignalFunc) gtk_print_cancel_button_cb,
		      printsel);
  gtk_container_add (GTK_CONTAINER (printsel->action_box),
		     printsel->cancel_button);
  
  gtk_widget_show_all (window_table);
  
}

static void
gtk_print_selection_destroy (GtkObject *object)
{
  g_return_if_fail (object != NULL);
  g_return_if_fail (GTK_IS_PRINT_SELECTION (object));

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gtk_print_set_radiobutton (GSList *list, gint value)
{
  GSList    *gsl;
  GtkWidget *tmp;
  gint      sel;

  gsl = list;
  while (gsl)
    {
      tmp = GTK_WIDGET (gsl->data);
      sel = GPOINTER_TO_INT(gtk_object_get_data (GTK_OBJECT (tmp), "value"));
      if (sel == value)
	{
	  gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (tmp), TRUE);
	  break;
	}
      gsl = gsl->next;
    }
}

/****************************************************************************
 * Callback functions
 ***************************************************************************/
static void
gtk_print_cancel_button_cb (GtkWidget *widget, GtkPrintSelection *sel)
{
  g_return_if_fail (GTK_IS_PRINT_SELECTION (sel));

  if (GTK_WIDGET_HAS_GRAB (sel))
    gtk_grab_remove (GTK_WIDGET (sel));
  gtk_widget_destroy (GTK_WIDGET (sel));
}
/*
 * Fileselector browsing...
 */
static void
gtk_print_browse_button_cb (GtkWidget *widget, GtkPrintSelection *printsel)
{  
  if (GTK_WIDGET_HAS_GRAB (printsel))
    {
      printsel->do_grab = TRUE;                /* Mark the state. */
      gtk_grab_remove (GTK_WIDGET (printsel));
    }
  if (printsel->filesel)
    {
      /* Already have one, pop it to the front. */
      gdk_window_show (printsel->filesel->window);
      gdk_window_raise (printsel->filesel->window);
      return;
    }
  else
    {
      gchar *filename;
      filename = gtk_entry_get_text (GTK_ENTRY (printsel->filename_entry));
      printsel->filesel = gtk_file_selection_new ("Select output file");
      
      gtk_file_selection_set_filename (GTK_FILE_SELECTION (printsel->filesel),
				       (filename) ? filename : "");
      gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (printsel->filesel)->cancel_button),
			  "clicked", (GtkSignalFunc) gtk_print_destroy_filesel,
			  printsel->filesel);
      gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (printsel->filesel)->ok_button),
			  "clicked", (GtkSignalFunc) gtk_print_filesel_ok_cb,
			  printsel);

      gtk_widget_show (printsel->filesel);
    }
}
static void
gtk_print_destroy_filesel (GtkWidget *widget, GtkFileSelection *filesel)
{
  g_return_if_fail (filesel != NULL);
  gtk_widget_destroy (GTK_WIDGET (filesel));
}
static void
gtk_print_filesel_ok_cb (GtkWidget *widget, GtkPrintSelection *printsel)
{
  gchar   *filename;
  g_return_if_fail (printsel != NULL);
  filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION 
					      (printsel->filesel));
  gtk_print_selection_set_filename (printsel, filename);
  gtk_widget_destroy (GTK_WIDGET (printsel->filesel));
  if (printsel->do_grab)
    gtk_grab_add (GTK_WIDGET (printsel));
}
static void
gtk_print_destination_cb (GtkWidget *widget, GtkPrintSelection *printsel)
{
  gint    new_selection;

  g_return_if_fail (GTK_IS_PRINT_SELECTION (printsel));

  new_selection = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (widget),
							"value"));
  switch (new_selection)
    {
    case GTK_PRINT_PRINTER:
      if ((GTK_TOGGLE_BUTTON (widget)->active))
	{
	  gtk_widget_set_sensitive (printsel->filename_entry, FALSE);
	  gtk_widget_set_sensitive (printsel->browse_button, FALSE);
	  gtk_widget_set_sensitive (printsel->command_entry, TRUE);
	}
      break;
    case GTK_PRINT_FILE:
      if ((GTK_TOGGLE_BUTTON (widget)->active))
	{
	  gtk_widget_set_sensitive (printsel->filename_entry, TRUE);
	  gtk_widget_set_sensitive (printsel->browse_button, TRUE);
	  gtk_widget_set_sensitive (printsel->command_entry, FALSE);
	}
      break;
    }
  printsel->print_destination = new_selection;
}
static void
gtk_print_method_cb (GtkWidget *widget, GtkPrintSelection *printsel)
{
  printsel->print_method = GPOINTER_TO_INT (gtk_object_get_data 
					    (GTK_OBJECT (widget), "value"));
}
static void 
gtk_print_orientation_cb (GtkWidget *widget, GtkPrintSelection *printsel)
{
  printsel->print_orientation = GPOINTER_TO_INT (gtk_object_get_data 
					       (GTK_OBJECT (widget), "value"));
}
static void
gtk_print_direction_cb (GtkWidget *widget, GtkPrintSelection *printsel)
{
  printsel->print_direction = GPOINTER_TO_INT (gtk_object_get_data
					       (GTK_OBJECT (widget), "value"));
}

/****************************************************************************
 * Public Functions
 ***************************************************************************/
/**
 * gtk_print_selection_new:
 * @title: The title that should be set in the resulting window.
 * 
 * This creates a new GtkFileSelection widget and returns a GtkWidget pointer.
 *
 * Return value: The newly-created item.
 **/
GtkWidget *
gtk_print_selection_new (const gchar *title)
{
  GtkPrintSelection *printsel;

  printsel = gtk_type_new (GTK_TYPE_PRINT_SELECTION);
  gtk_window_set_title (GTK_WINDOW (printsel), title);
  return GTK_WIDGET (printsel);
}
/**
 * gtk_print_selection_set_command:
 * @printsel: The previously created print dialog.
 * @command: The string to copy into the command entry widget.
 *
 * This will set the command string for output to the printer.
 **/
void
gtk_print_selection_set_command (GtkPrintSelection *printsel, 
				 const gchar *command)
{
  g_return_if_fail (GTK_IS_PRINT_SELECTION (printsel));
  g_return_if_fail (command != NULL);
  
  gtk_entry_set_text (GTK_ENTRY (printsel->command_entry), command);
}
/**
 * gtk_print_selection_get_command:
 * @printsel: The previously created @GtkPrintSelection widget.
 *
 **/
gchar *
gtk_print_selection_get_command (GtkPrintSelection *printsel)
{
  g_return_val_if_fail (printsel != NULL, NULL);
  g_return_val_if_fail (GTK_IS_PRINT_SELECTION (printsel), NULL);

  return gtk_entry_get_text (GTK_ENTRY (printsel->command_entry));
}
/**
 * gtk_print_selection_set_filename:
 * @printsel: The previously created print dialog.
 * @filename: The string to copy into the filename entry widget.
 *
 * This will set the filename for output to a file.
 **/
void
gtk_print_selection_set_filename (GtkPrintSelection *printsel,
				  const gchar *filename)
{
  g_return_if_fail (printsel != NULL);
  g_return_if_fail (GTK_IS_PRINT_SELECTION (printsel));
  g_return_if_fail (filename != NULL);
  
  gtk_entry_set_text (GTK_ENTRY (printsel->filename_entry), filename);
}
/** 
 * gtk_print_selection_get_filename:
 * @printsel: The previously created @GtkPrintSelection widget.
 *
 **/
gchar *
gtk_print_selection_get_filename (GtkPrintSelection *printsel)
{
  g_return_val_if_fail (printsel != NULL, NULL);
  g_return_val_if_fail (GTK_IS_PRINT_SELECTION (printsel), NULL);
  
  return gtk_entry_get_text (GTK_ENTRY (printsel->filename_entry));
}
/**
 * gtk_print_selection_set_destination:
 * @printsel: The previously created printselection widget.
 *
 * This will set the print direction under program control.
 *
 **/
void 
gtk_print_selection_set_destination (GtkPrintSelection *printsel, 
				     GtkPrintDestination dest)
{
  g_return_if_fail (printsel != NULL);
  g_return_if_fail (GTK_IS_PRINT_SELECTION (printsel));

  gtk_print_set_radiobutton (printsel->destination_group, dest);
}
gint
gtk_print_selection_get_destination (GtkPrintSelection *printsel)
{
  g_return_val_if_fail (printsel != NULL, -1);
  g_return_val_if_fail (GTK_IS_PRINT_SELECTION (printsel), -1);

  return printsel->print_destination;
}
void 
gtk_print_selection_set_method (GtkPrintSelection *printsel,
				GtkPrintMethod method)
{
  g_return_if_fail (printsel != NULL);
  g_return_if_fail (GTK_IS_PRINT_SELECTION (printsel));

  gtk_print_set_radiobutton (printsel->method_group, method);
}
gint 
gtk_print_selection_get_method (GtkPrintSelection *printsel)
{
  g_return_val_if_fail (printsel != NULL, -1);
  g_return_val_if_fail (GTK_IS_PRINT_SELECTION (printsel), -1);

  return printsel->print_method;
}
void 
gtk_print_selection_set_orientation (GtkPrintSelection *printsel,
				     GtkPrintOrientation orientation)
{
  g_return_if_fail (printsel != NULL);
  g_return_if_fail (GTK_IS_PRINT_SELECTION (printsel));

  gtk_print_set_radiobutton (printsel->orientation_group, orientation);
}
gint 
gtk_print_selection_get_orientation (GtkPrintSelection *printsel)
{
  g_return_val_if_fail (printsel != NULL, -1);
  g_return_val_if_fail (GTK_IS_PRINT_SELECTION (printsel), -1);

  return printsel->print_orientation;
}
void
gtk_print_selection_set_direction (GtkPrintSelection *printsel,
				   GtkPrintDirection direction)
{
  g_return_if_fail (printsel != NULL);
  g_return_if_fail (GTK_IS_PRINT_SELECTION (printsel));

  gtk_print_set_radiobutton (printsel->direction_group, direction);
}
gint 
gtk_print_selection_get_direction (GtkPrintSelection *printsel)
{
  g_return_val_if_fail (printsel != NULL, -1);
  g_return_val_if_fail (GTK_IS_PRINT_SELECTION (printsel), -1);

  return printsel->print_direction;
}  
/**
 * gtk_print_selection_get_definition:
 * @printsel: The previously created print dialog.
 * 
 * This function will return the GtkPrinterDef described by the dialog.
 *
 **/
GtkPrinterDef *
gtk_print_selection_get_definition (GtkPrintSelection *printsel)
{
  GtkPrinterDef   *ret_val;

  g_return_val_if_fail (printsel != NULL, NULL);
  g_return_val_if_fail (GTK_IS_PRINT_SELECTION (printsel), NULL);

  if (!(ret_val = g_malloc (sizeof (GtkPrinterDef))) )
    return NULL;
  ret_val->print_destination = printsel->print_destination;
  ret_val->print_direction   = printsel->print_direction;
  ret_val->print_method      = printsel->print_method;
  ret_val->print_orientation = printsel->print_orientation;
  ret_val->output_filename   = g_strdup (gtk_entry_get_text (GTK_ENTRY (printsel->filename_entry)));
  ret_val->output_command    = g_strdup (gtk_entry_get_text (GTK_ENTRY (printsel->command_entry)));
  return ret_val;
}

/* EOF */
