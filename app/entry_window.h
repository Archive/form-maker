/* -*- Mode: C -*-
 * $Id$
 * Form Maker  -- A premade form filler outer.
 * Copyright 1999, ComStar Communications Corp.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * This widget implements the data entry window.
 * This is only used in the interactive mode.
 */
#ifndef __ENTRY_WINDOW_H__
#define __ENTRY_WINDOW_H__
BEGIN_GNOME_DECLS
#include <gnome.h>
#include <glade/glade.h>

#include "worksheet.h"
/***************************************************************************
 * Standard Widget macros
 ****************************************************************************/
#define ENTRY_WINDOW_TYPE (entry_window_get_type ())
#define ENTRY_WINDOW(obj) \
        GTK_CHECK_CAST ((obj), ENTRY_WINDOW_TYPE, entry_window)
#define ENTRY_WINDOW_CLASS(klas) \
        GTK_CHECK_CLASS_CAST ((klas), ENTRY_WINDOW_TYPE, entry_window_class)
#define IS_ENTRY_WINDOW(obj) \
        GTK_CHECK_TYPE ((obj), ENTRY_WINDOW_TYPE)
#define IS_ENTRY_WINDOW_CLASS(klas) \
        GTK_CHECK_CLASS_TYPE ( (klas), ENTRY_WINDOW_TYPE)
/****************************************************************************
 * Control Blocks for the widget class and for widget instances.
 ***************************************************************************/
typedef struct _entry_window          entry_window;
typedef struct _entry_window_class    entry_window_class;
struct _entry_window
{
  GnomeApp     gnomeapp;

  forms_def    *form;
  GladeXML     *xml;

};
struct _entry_window_class
{
  GnomeAppClass   parent_class;
};
/****************************************************************************
 * Public widget manipulation functions
 ***************************************************************************/
GtkType        entry_window_get_type                 (void);
GtkWidget      *entry_window_new                     (forms_def  *form);
void           clear_entry_window                    (GtkWidget  *widget);
END_GNOME_DECLS
#endif
/* EOF */

