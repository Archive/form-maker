/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 */
 
#ifndef __DIALOG_H__
#define __DIALOG_H__ 
#include <config.h>
#include <sys/types.h>
#include <gtk/gtk.h>

GtkWidget            *printing_dialog_open      (void);
void                 printing_dialog_close      (GtkWidget    *widget);
void                 printing_dialog_update     (GtkWidget    *widget,
						 gfloat       percent);
GtkWidget            *open_fileselection        (GtkWidget    *parent,
						 gboolean     parent_grab);
void                 close_fileselection        (GtkWidget    *widget);
GtkWidget            *create_print_window       (GtkWidget    *widget);
gboolean             confirmation_dialog        (gchar *msg, ...)
                                                __attribute__ 
                                                ((format (printf, 1, 2)));
void                 notice_dialog              (gboolean modal, 
						 gchar *msg,
						 ...)
                                                __attribute__
                                                ((format (printf, 2, 3)));

/*
 * Printer output defines.
 */
#define PRINTER_OUTPUT	0
#define FILE_OUTPUT     1
/*
 * Orientation defines.
 */
#define PORTRAIT        0
#define LANDSCAPE       1

/*
 * Print type defines.
 */
#define PRINT_COLOR     0
#define PRINT_GREY      1

/*
 * Print direction defines.
 */
#define PRINT_FORWARD   0
#define PRINT_REVERSE   1

/* 
 * Kludgy but hey I can't find this elsewhere and I'd rather not hunt the 
 * for it.
 */
typedef struct __paper_table {
  char     *paper_name;           /* Name of this paper (letter,legal etc.) */
  gint     paper_width;
  gint     paper_height;          /* Size in points */
} paper_table;

typedef struct __print_desc {
  char     *command;              /* command */
  char     *out_file;             /* filename output */
  char     *paper_name;           /* Output on this size paper */
  char     *temp_filename;        /* When we spool to the printer this is the 
				     file that we write to before sending. */
  gint     destination;           /* printer or file */
  gint     orientation;           /* Portrait or Landscape? */
  gint     direction;             /* forward or reverse */
  gboolean color;                 /* color output */
} print_desc;

paper_table           *paper_size                (const char  *name);

/* Forward declarations */
void         set_widget_text                     (GtkWidget     *widget,
						  gchar         *text);
gchar        *get_widget_text                    (GtkWidget     *widget);
#endif

/* EOF */

