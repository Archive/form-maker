/* -*- Mode: C -*-
 * $Id$
 * Copyright (C) 1998, 1999 Gregory McLean
 * Author: Gregory McLean <gregm@comstar.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * Handles Adding deleteing and editing the form definitions
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <gnome.h>

#include "worksheet.h"
#include "gtkutil.h"
#include "editor.h"
#include "dialog.h"
#include "field_edit.h"

static void        close_button_cb             (GtkWidget     *widget,
						gpointer      data);
static void        edit_field_button_cb        (GtkWidget     *widget,
						gpointer      data);
static void        add_field_button_cb         (GtkWidget     *widget,
						gpointer      data);
static void        delete_field_button_cb      (GtkWidget     *widget,
						gpointer      data);
static void        save_button_cb              (GtkWidget     *widget,
						gpointer      data);
static void        browse_button_cb            (GtkWidget     *widget,
						gpointer      data);
static void        group_entry_cb              (GtkWidget     *widget,
						gpointer      data);
static void        browse_ok_button_cb         (GtkWidget     *widget,
						gpointer      data);
static void        save_ok_button_cb           (GtkWidget     *widget,
						gpointer      data);
static void        field_cancel_button_cb      (GtkWidget     *widget,
						gpointer      data);
static void        field_ok_button_cb          (GtkWidget     *widget,
						gpointer      data);
static GtkWidget *add_form_window = NULL;

/****************************************************************************
 * form_panel () -- The actual form definition editor.
 ***************************************************************************/
GtkWidget *
form_panel (const char *which)
{
  GtkWidget *window_frame;
  GtkWidget *window_table;
  GtkWidget *scrolled;
  GtkWidget *field_list;
  GtkWidget *label;
  GtkWidget *frame3;
  GtkWidget *vbuttonbox1;
  GtkWidget *button;
  GtkWidget *frame2;
  GtkWidget *hbuttonbox1;
  GtkWidget *entry;
  GtkWidget *label2;
  GtkWidget *form_name;
  GtkWidget *label1;

  add_form_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_container_set_border_width(GTK_CONTAINER(add_form_window), 3);
  gtk_window_set_title(GTK_WINDOW(add_form_window), which);
  gtk_widget_set_name (GTK_WIDGET (add_form_window), "add_form_window");
  window_frame = gtk_frame_new(NULL);
  gtk_container_add(GTK_CONTAINER(add_form_window), window_frame);
  gtk_container_set_border_width(GTK_CONTAINER(window_frame), 2);
  
  window_table = gtk_table_new(6, 5, FALSE);
  gtk_table_set_row_spacings(GTK_TABLE(window_table), 1);
  gtk_table_set_col_spacings(GTK_TABLE(window_table), 1);
  gtk_container_add(GTK_CONTAINER(window_frame), window_table);
  
  scrolled = gtk_scrolled_window_new(NULL, NULL);
  gtk_container_set_border_width(GTK_CONTAINER(scrolled), 2);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled),
                                 GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
  gtk_table_attach(GTK_TABLE(window_table), scrolled, 0, 4, 2, 5,
                   GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 3, 3);
  
  field_list = gtk_list_new();
  gtk_widget_set_name (GTK_WIDGET (field_list), "FORM_MAKER_field_list");
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrolled), 
					field_list);
  gtk_object_set_data (GTK_OBJECT (add_form_window), "field_list", field_list);
  button = gtk_button_new_with_label("Browse...");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) browse_button_cb,
		      add_form_window);
  gtk_table_attach(GTK_TABLE(window_table), button, 4, 5, 1, 2,
                   GTK_FILL, GTK_FILL, 4, 0);
  
  entry = gtk_entry_new_with_max_length (255);
  gtk_table_attach(GTK_TABLE(window_table), entry, 3, 4, 1, 2,
                   GTK_FILL, GTK_FILL, 4, 0);
  gtk_object_set_data (GTK_OBJECT (add_form_window), "template_path", entry);
  label = gtk_label_new("Template Path");
  gtk_table_attach(GTK_TABLE(window_table), label, 2, 3, 1, 2,
                   GTK_FILL, GTK_FILL, 4, 0);
  
  label = gtk_label_new("Fields");
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.0);
  gtk_table_attach(GTK_TABLE(window_table), label, 
		   0, 1, 1, 2,
                   GTK_FILL, GTK_FILL, 4, 0);
  
  frame3 = gtk_frame_new(NULL);
  gtk_table_attach(GTK_TABLE(window_table), frame3, 4, 5, 2, 5,
                   GTK_FILL, GTK_FILL, 0, 0);
  gtk_container_set_border_width(GTK_CONTAINER(frame3), 2);
  
  vbuttonbox1 = gtk_vbutton_box_new();
  gtk_container_add(GTK_CONTAINER(frame3), vbuttonbox1);
  gtk_container_set_border_width(GTK_CONTAINER(vbuttonbox1), 4);
  gtk_button_box_set_layout(GTK_BUTTON_BOX(vbuttonbox1), GTK_BUTTONBOX_START);
  
  button = gtk_button_new_with_label("Add Field...");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) add_field_button_cb,
		      add_form_window);
  gtk_container_add(GTK_CONTAINER(vbuttonbox1), button);
  
  button = gtk_button_new_with_label("Delete Field...");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) delete_field_button_cb,
		      add_form_window);
  gtk_container_add(GTK_CONTAINER(vbuttonbox1), button);
  
  button = gtk_button_new_with_label("Edit Field...");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) edit_field_button_cb,
		      add_form_window);
  gtk_container_add(GTK_CONTAINER(vbuttonbox1), button);
  
  frame2 = gtk_frame_new(NULL);
  gtk_container_set_border_width (GTK_CONTAINER (frame2), 1);
  gtk_table_attach(GTK_TABLE(window_table), frame2, 0, 5, 5, 6,
                   GTK_FILL, GTK_FILL, 3, 2);
  
  hbuttonbox1 = gtk_hbutton_box_new();
  gtk_container_add(GTK_CONTAINER(frame2), hbuttonbox1);
  gtk_container_set_border_width(GTK_CONTAINER(hbuttonbox1), 6);
  gtk_button_box_set_layout(GTK_BUTTON_BOX(hbuttonbox1), GTK_BUTTONBOX_END);
  
  button = gtk_button_new_with_label("Save");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) save_button_cb,
		      (GtkWidget *)add_form_window);
  gtk_container_add(GTK_CONTAINER(hbuttonbox1), button);
  
  button = gtk_button_new_with_label("Close");
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
                     GTK_SIGNAL_FUNC(close_button_cb),
                     add_form_window);
  gtk_container_add(GTK_CONTAINER(hbuttonbox1), button);
  
  entry = gtk_combo_new();
  gtk_combo_set_case_sensitive ( GTK_COMBO(entry), FALSE);
  gtk_table_attach(GTK_TABLE(window_table), entry, 
		   3, 5, 0, 1,
                   GTK_FILL, GTK_FILL, 6, 3);
  gtk_object_set_data (GTK_OBJECT (add_form_window), "form_group", entry);
  build_group_list (entry);
  /*
   * Connect up the signals to a function.
   */
  gtk_signal_connect (GTK_OBJECT (GTK_COMBO(entry)->entry), "activate",
		      (GtkSignalFunc) group_entry_cb,
		      add_form_window);
  gtk_signal_connect (GTK_OBJECT (GTK_COMBO(entry)->entry), "focus_out_event",
		      (GtkSignalFunc) group_entry_cb,
		      add_form_window);
  gtk_signal_connect (GTK_OBJECT (GTK_COMBO(entry)->entry), "changed",
		      (GtkSignalFunc) group_entry_cb,
		      add_form_window);
  label2 = gtk_label_new("Group");
  gtk_misc_set_alignment (GTK_MISC (label2), 0.1, 0.5);
  gtk_table_attach(GTK_TABLE(window_table), label2, 2, 3, 0, 1,
		   GTK_FILL, GTK_FILL, 0, 0);
  
  form_name = gtk_entry_new();
  gtk_table_attach(GTK_TABLE(window_table), form_name, 1, 2, 0, 1,
                   GTK_FILL, GTK_FILL, 4, 0);
  gtk_object_set_data (GTK_OBJECT (add_form_window), "form_name", form_name);
  label1 = gtk_label_new("Form Name");
  gtk_table_attach(GTK_TABLE(window_table), label1, 0, 1, 0, 1,
                   GTK_FILL, GTK_FILL, 4, 0);
  gtk_object_set_data (GTK_OBJECT (add_form_window), "parent_grab", 
		       (gboolean *)FALSE);
  gtk_object_set_data (GTK_OBJECT (add_form_window), "invoker_widget",
		       GTK_WIDGET (add_form_window));
  gtk_widget_show_all (add_form_window);
  return add_form_window;
}
/****************************************************************************
 * field_form () -- Field definition editing.
 ***************************************************************************/
GtkWidget*
field_form (const char *title)
{
  GtkWidget *window;                 /* The window for this panel */
  GtkWidget *frame;                  /* The frame for the panel. */
  GtkWidget *vbox;                   /* The vbox all this is stuffed in. */
  GtkWidget *hbuttonbox1;
  GtkWidget *button;
  GtkWidget *hseparator1;
  GtkWidget *label;
  GtkWidget *entry;
  GtkWidget *menu;
  GtkWidget *menuitem;
  GtkWidget *spinbutton1;
  GtkWidget *field_edit;


  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_container_set_border_width (GTK_CONTAINER(window), 4);
  gtk_window_set_title (GTK_WINDOW(window), title);
  

  frame = gtk_frame_new (NULL);
  gtk_container_set_border_width (GTK_CONTAINER (frame), 2);
  gtk_container_add (GTK_CONTAINER(window), frame);

  field_edit = field_edit_new ();

  gtk_container_add (GTK_CONTAINER (frame), field_edit);
  gtk_object_set_data (GTK_OBJECT (window), "field_edit", field_edit);
#if 0
  table1 = gtk_table_new(8, 4, FALSE);
  gtk_container_add(GTK_CONTAINER(frame), table1);
  gtk_container_set_border_width(GTK_CONTAINER(table1), 4);
  gtk_table_set_row_spacings(GTK_TABLE(table1), 4);

  frame = gtk_frame_new(NULL);
  gtk_table_attach(GTK_TABLE(table1), frame, 
		   0, 2, 7, 8,
                   GTK_FILL, GTK_FILL, 0, 0);
  gtk_container_set_border_width(GTK_CONTAINER(frame), 4);

  hbuttonbox1 = gtk_hbutton_box_new();
  gtk_container_add(GTK_CONTAINER(frame), hbuttonbox1);
  gtk_container_set_border_width(GTK_CONTAINER(hbuttonbox1), 5);

  button = gtk_button_new_with_label("Ok");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) field_ok_button_cb,
		      window1);
  gtk_container_add(GTK_CONTAINER(hbuttonbox1), button);

  button = gtk_button_new_with_label("Cancel");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      (GtkSignalFunc) field_cancel_button_cb,
		      window1);
  gtk_container_add(GTK_CONTAINER(hbuttonbox1), button);

  hseparator1 = gtk_hseparator_new();
  gtk_table_attach(GTK_TABLE(table1), hseparator1, 
		   0, 2, 6, 7,
                   GTK_FILL, GTK_FILL, 0, 0);

  label = gtk_label_new("Field");
  gtk_table_attach(GTK_TABLE(table1), label, 0, 1, 0, 1,
                   GTK_FILL, GTK_FILL, 0, 0);
  entry = gtk_entry_new();
  gtk_object_set_data(GTK_OBJECT(window1), "field_macro", entry);
  gtk_table_attach(GTK_TABLE(table1), entry, 
		   1, 2, 0, 1,
                   GTK_FILL, GTK_FILL, 0, 0);

  label = gtk_label_new("Macro");
  gtk_table_attach(GTK_TABLE(table1), label, 0, 1, 1, 2,
                   GTK_FILL, GTK_FILL, 0, 0);
  entry = gtk_entry_new();
  gtk_object_set_data(GTK_OBJECT(window1), "field_macro", entry);
  gtk_table_attach(GTK_TABLE(table1), entry, 1, 2, 1, 2,
                   GTK_FILL, GTK_FILL, 0, 0);
  label = gtk_label_new ("Default Value");
  gtk_table_attach (GTK_TABLE (table1), label, 
		    0, 1, 2, 3,
		    GTK_FILL, GTK_FILL, 0, 0);
  entry = gtk_entry_new ();
  gtk_object_set_data (GTK_OBJECT (window1), "field_default", entry);
  gtk_table_attach (GTK_TABLE (table1), entry,
		    1, 2, 2, 3,
		    GTK_FILL, GTK_FILL, 0, 0);

  label = gtk_label_new ("Tooltip");
  gtk_table_attach(GTK_TABLE(table1), label, 
		   0, 1, 3, 4,
                   GTK_FILL, GTK_FILL, 0, 0);
  entry = gtk_entry_new();
  gtk_object_set_data (GTK_OBJECT(window1), "field_tooltip", entry);
  gtk_table_attach(GTK_TABLE(table1), entry, 
		   1, 2, 3, 4,
                   GTK_FILL, GTK_FILL, 0, 0);
  /* --- ENTRY TYPES --- */
  label = gtk_label_new("Type");
  gtk_table_attach(GTK_TABLE(table1), label, 
		   0, 1, 4, 5,
                   GTK_FILL, GTK_FILL, 0, 0);
  entry = gtk_option_menu_new();
  menu = gtk_menu_new ();
  menuitem = gtk_menu_item_new_with_label ("Text Entry");
  gtk_object_set_data (GTK_OBJECT (menuitem), "selection", 
		       (int *)FIELD_TEXT_ENTRY);
  gtk_menu_append (GTK_MENU (menu), menuitem);
  menuitem = gtk_menu_item_new_with_label ("Numeric Entry (spinbutton)");
  gtk_object_set_data (GTK_OBJECT (menuitem), "selection",
		       (int *)FIELD_NUMS_ENTRY);
  gtk_menu_append (GTK_MENU (menu), menuitem);
  gtk_option_menu_set_menu (GTK_OPTION_MENU (entry), menu);
  gtk_object_set_data(GTK_OBJECT(window1), "field_type", entry);
  gtk_table_attach(GTK_TABLE(table1), entry, 
		   1, 2, 4, 5,
                   GTK_FILL, GTK_FILL, 0, 0);
  /* --- SIZE --- */
  label = gtk_label_new("Size");
  gtk_table_attach(GTK_TABLE(table1), label, 
		   0, 1, 5, 6,
                   GTK_FILL, GTK_FILL, 0, 0);
  adjustment = GTK_ADJUSTMENT(gtk_adjustment_new ( 1.0, 1.0, 255.0, 1.0, 
						   1.0, 1.0));
  spinbutton1 = gtk_spin_button_new(adjustment, 1, 0);
  gtk_object_set_data(GTK_OBJECT(window1), "field_size", spinbutton1);
  gtk_widget_show(spinbutton1);
  gtk_table_attach(GTK_TABLE(table1), spinbutton1, 
		   1, 2, 5, 6,
                   GTK_FILL, GTK_FILL, 0, 0);
  gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(spinbutton1), TRUE);
#endif
  gtk_widget_show_all (window);
  return window;
}


static void
close_button_cb (GtkWidget *widget, gpointer data)
{
  g_return_if_fail (widget != NULL);
  g_return_if_fail (data != NULL);

  gtk_widget_destroy (GTK_WIDGET (data));
  free_group_list ();
}

static void
delete_field_button_cb (GtkWidget *widget, gpointer data)
{
  GtkObject   *list_item;
  GtkWidget   *temp;
  GList       *dlist;
  field_def   *field;
  field_def   *field_tmp;
  forms_def    *form;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_OBJECT (data));
  
  temp = (GtkWidget *)get_widget (data, "field_list");
  g_return_if_fail (GTK_IS_LIST (temp));
  dlist = GTK_LIST (temp)->selection;
  form = (forms_def *) gtk_object_get_data (GTK_OBJECT (data),
					    "forms_def");
  if (dlist)
    {
      list_item = GTK_OBJECT (dlist->data);
      field = (field_def *)gtk_object_get_data (GTK_OBJECT (list_item),
						"field_def");
      /* 
       * Remove the visual part
       */
      gtk_list_remove_items (GTK_LIST (temp), dlist);
      dlist = (GList *)form->field_list;
      while (dlist)
	{
	  field_tmp = (field_def *)dlist->data;
	  if (strcasecmp (field_tmp->field_name, field->field_name) == 0)
	    {
	      form->field_list = g_slist_remove (form->field_list, field_tmp);
	      break;
	    }
	  dlist = dlist->next;
	}
    }
  else
    {
      notice_dialog (FALSE, "No field selected to delete!");
    }
}

static void 
save_button_cb (GtkWidget *widget, gpointer data)
{
  GtkWidget   *fileselection;
  forms_def   *form;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_WIDGET (data));

  form = (forms_def *)gtk_object_get_data (GTK_OBJECT (data), "forms_def");
  if (form)
    {
      gtk_object_set_data (data, "file_name", form->cfg_path);
      fileselection = (GtkWidget *)open_fileselection ((GtkWidget *)data, 
						       FALSE);
      gtk_signal_connect (GTK_OBJECT
			  (GTK_FILE_SELECTION (fileselection)->ok_button),
			  "clicked",
			  (GtkSignalFunc) save_ok_button_cb,
			  fileselection);
    }
}

static void
browse_button_cb (GtkWidget *widget, gpointer data)
{
  GtkWidget  *fileselection;
  forms_def  *form;
  
  g_return_if_fail (data != NULL);
  g_return_if_fail (widget != NULL);

  form = gtk_object_get_data (GTK_OBJECT (data), "forms_def");
  if (form)
    gtk_object_set_data (GTK_OBJECT (data), "file_name", form->path);
  fileselection = (GtkWidget *)open_fileselection (data, FALSE);
  gtk_signal_connect (GTK_OBJECT 
		      (GTK_FILE_SELECTION (fileselection)->ok_button),
		      "clicked",
		      (GtkSignalFunc) browse_ok_button_cb,
		      (GtkWidget *)fileselection);
}

static void
group_entry_cb (GtkWidget *widget, gpointer data)
{
  GtkWidget   *parent;
  GtkWidget   *temp;
  forms_def   *form;
  char        *new_group;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (data != NULL);

  if (widget->parent)
    parent = gtk_widget_get_toplevel (widget);
  else
    parent = widget;
  form = (forms_def *)gtk_object_get_data (GTK_OBJECT (parent), "forms_def");
  new_group = gtk_entry_get_text (GTK_ENTRY (widget));
  if (form)
    {
      if (strcasecmp (new_group, form->group) != 0)
	g_print ("Group changed %s -> %s\n", 
		 form->group,
		 new_group);
    }
}

static void
save_ok_button_cb (GtkWidget *widget, gpointer data)
{
  GtkWidget   *fileselection;
  GtkWidget   *invoker;
  gchar       *pathname;
  forms_def   *form;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_OBJECT (data));
  g_return_if_fail (GTK_IS_FILE_SELECTION (data));

  fileselection = GTK_WIDGET (data);
  gtk_widget_hide (fileselection);
  pathname = g_strdup (gtk_file_selection_get_filename 
		       (GTK_FILE_SELECTION (fileselection)));
  invoker = (GtkWidget *)get_widget(data, "invoker_widget");
  form = gtk_object_get_data (GTK_OBJECT (invoker), "forms_def");
  save_form_config (pathname, form);
  /* close the file requestor. */
  gtk_widget_destroy (GTK_WIDGET (data));
  /* close the form editor */
  gtk_widget_destroy (GTK_WIDGET (invoker));
}

static void
browse_ok_button_cb (GtkWidget *widget, gpointer data)
{
  GtkWidget   *fileselection;
  GtkWidget   *invoker;
  GtkWidget   *temp;
  forms_def   *form;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_OBJECT (data));

  fileselection = GTK_WIDGET (data);
  if (fileselection)
    {
      invoker = (GtkWidget *)get_widget (data, "invoker_widget");
      temp = (GtkWidget *)get_widget (invoker, "template_path");
      gtk_entry_set_text (GTK_ENTRY (temp), gtk_file_selection_get_filename
			  (GTK_FILE_SELECTION (fileselection)));
      form = gtk_object_get_data (GTK_OBJECT (invoker), "forms_def");
      if (form)
	{
	  form->path = g_strdup (gtk_file_selection_get_filename 
				 (GTK_FILE_SELECTION (fileselection)));
	}
    }
  gtk_widget_destroy (GTK_WIDGET (data));
}

static void
edit_field_button_cb (GtkWidget *widget, gpointer data)
{
  GtkObject *list_item;
  GtkWidget *temp;
  GtkWidget *dialog;
  GtkWidget *field_edit;
  GList     *dlist;
  field_def *field;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_WIDGET (data));

  temp = (GtkWidget *)get_widget ( GTK_WIDGET (data), "field_list");
  if (temp == NULL)
    return;

  dlist = GTK_LIST (temp)->selection;
  if (dlist)
    {
      list_item = GTK_OBJECT (dlist->data);
      field = (field_def *)gtk_object_get_data (GTK_OBJECT (list_item),
						"field_def");
      g_print ("Open dialog..\n");
      dialog = field_form ("Edit Field");
      field_edit = get_widget (dialog, "field_edit");
      field_edit_put_data (field_edit, field);
#if 0
      gtk_object_set_data (GTK_OBJECT (dialog), "edit_field", 
			   (gboolean *)TRUE);
      temp = GTK_WIDGET (get_widget (dialog, "field_macro"));
      gtk_entry_set_text (GTK_ENTRY (temp), 
			  (field->field_name) ? field->field_name : "");
      temp = GTK_WIDGET (get_widget (dialog, "field_macro"));
      gtk_entry_set_text (GTK_ENTRY (temp), 
			  (field->field_macro) ?  field->field_macro : "");
      temp = GTK_WIDGET (get_widget (dialog, "field_tooltip"));
      gtk_entry_set_text (GTK_ENTRY (temp), 
			  (field->field_tip) ? field->field_tip : "");
      temp = GTK_WIDGET (get_widget (dialog, "field_default"));
      gtk_entry_set_text (GTK_ENTRY (temp), 
			  (field->field_default) ? field->field_default : "");
      temp = GTK_WIDGET (get_widget (dialog, "field_size"));
      gtk_spin_button_set_value (GTK_SPIN_BUTTON (temp), field->field_width);
#endif
      gtk_object_set_data (GTK_OBJECT (dialog), "field_def", 
			   (field_def *) field);
      gtk_object_set_data (GTK_OBJECT (dialog), "invoker_widget",
			   data);
    }
}

static void
add_field_button_cb (GtkWidget *widget, gpointer data)
{
  GtkWidget   *dialog;
  
  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_OBJECT (data));
  dialog = field_form ("Add a form");

  /*
   * Prime the dialog with a bit of state info
   */
  gtk_object_set_data (GTK_OBJECT (dialog), "invoker_widget", data);
  gtk_object_set_data (GTK_OBJECT (dialog), "edit_field", (gboolean *)FALSE);
}

static void 
field_cancel_button_cb (GtkWidget *widget, gpointer data)
{
  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_WIDGET (data));

  gtk_widget_destroy (GTK_WIDGET (data));
}

static void
field_ok_button_cb (GtkWidget *widget, gpointer data)
{
  GtkWidget  *temp;
  GtkWidget  *invoker;
  GtkWidget  *list_item;
  GtkWidget  *field_list;
  GList      *dlist;
  field_def  *field;
  forms_def  *form;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_OBJECT (data));
  g_return_if_fail (data != NULL);
#if 0
  if (gtk_object_get_data (GTK_OBJECT (data), "edit_field"))
    {
      g_print ("Edit field!\n");

      field = (field_def *)gtk_object_get_data (GTK_OBJECT (data), 
						"field_def");
      temp = (GtkWidget *)get_widget (data, "field_macro");
      field->field_name = g_strdup (gtk_entry_get_text (GTK_ENTRY (temp)));
      temp = (GtkWidget *)get_widget (data, "field_macro");
      field->field_macro = g_strdup (gtk_entry_get_text (GTK_ENTRY (temp)));
      temp = (GtkWidget *)get_widget (data, "field_tooltip");
      field->field_tip = g_strdup (gtk_entry_get_text (GTK_ENTRY (temp)));
      temp = (GtkWidget *)get_widget (data, "field_default");
      field->field_default = g_strdup (gtk_entry_get_text (GTK_ENTRY (temp)));
      temp = (GtkWidget *)get_widget (data, "field_size");
      field->field_width = gtk_spin_button_get_value_as_int 
	(GTK_SPIN_BUTTON (temp));
      invoker = (GtkWidget *)get_widget (data, "invoker_widget");
      g_return_if_fail (invoker != NULL);
      temp = (GtkWidget *)get_widget (invoker, "field_list");
      if (temp)
	{
	  dlist = GTK_LIST (temp)->selection;
	  if (dlist)
	    {
	      gtk_list_remove_items (GTK_LIST (temp), dlist);
	      list_item = gtk_list_item_new_with_label (field->field_name);
	      gtk_object_set_data (GTK_OBJECT (list_item), "field_def", field);
	      gtk_widget_show (list_item);
	      gtk_container_add (GTK_CONTAINER (temp), list_item); 
	      
	    }
	}
    }
  else
    {
      invoker = (GtkWidget *)get_widget (data, "invoker_widget");
      field_list = (GtkWidget *)get_widget (invoker, "field_list");
      form = (forms_def *)gtk_object_get_data (GTK_OBJECT (invoker), 
					       "forms_def");
      field = g_new0 (field_def, 1);
      temp = (GtkWidget *)get_widget (data, "field_macro");
      field->field_name = g_strdup (gtk_entry_get_text (GTK_ENTRY (temp)));
      temp = (GtkWidget *)get_widget (data, "field_macro");
      field->field_macro = g_strdup (gtk_entry_get_text (GTK_ENTRY (temp)));
      temp = (GtkWidget *)get_widget (data, "field_default");
      field->field_default = g_strdup (gtk_entry_get_text (GTK_ENTRY (temp)));
      temp = (GtkWidget *)get_widget (data, "field_size");
      field->field_width = gtk_spin_button_get_value_as_int 
	(GTK_SPIN_BUTTON (temp));
      form->field_list = g_slist_append (form->field_list, field);
      list_item = gtk_list_item_new_with_label (field->field_name);
      form->field_count++;
      gtk_object_set_data (GTK_OBJECT (list_item), "field_def", field);
      gtk_widget_show (list_item);
      gtk_container_add (GTK_CONTAINER (field_list), list_item);

    }
#endif
  gtk_widget_destroy (GTK_WIDGET (data));
}

/* EOF */



