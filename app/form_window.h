/* -*- Mode: C -*-
 * $Id$
 * Form Maker  -- A premade form filler outer.
 * Copyright 1999 Gregory McLean
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,  59 Temple Place - Suite 330, Cambridge, MA 02139, USA.
 *
 * This widget implements the main chooser window listing all forms avaliable
 * and the groups they are in. This is only used in the interactive mode.
 */
#ifndef __FORM_WINDOW_H__
#define __FORM_WINDOW_H__
BEGIN_GNOME_DECLS
#include "worksheet.h"
/****************************************************************************
 * Standard widget macros
 ***************************************************************************/
#define FORM_WINDOW_TYPE (form_window_get_type())
#define FORM_WINDOW(obj) \
        GTK_CHECK_CAST ((obj), FORM_WINDOW_TYPE, form_window)
#define FORM_WINDOW_CLASS(klas) \
        GTK_CHECK_CLASS_CAST (klass, FORM_WINDOW_TYPE, form_window_class)
#define IS_FORM_WINDOW(obj) \
        GTK_CHECK_TYPE ((obj), FORM_WINDOW_TYPE)
#define IS_FORM_WINDOW_CLASS(klass) \
        GTK_CHECK_CLASS_TYPE ((klass), FORM_WINDOW_TYPE)
/****************************************************************************
 * Control blocks for the widget class and for widget instances 
 ***************************************************************************/
typedef struct _form_window                 form_window;
typedef struct _form_window_class           form_window_class;
struct _form_window
{
  GnomeApp       gnomeapp;                   /* The GnomeApp base widget */
  
  GtkWidget      *form_notebook;             /* The notebook of forms */
  GtkWidget      *complete_button;      
  GtkWidget      *edit_button;              
  GtkWidget      *delete_button;
  GList          *form_list;                 /* List of forms */
  GHashTable     *form_groups;               /* Hash of the groups of forms */
 
  gpointer       selected_form;              /* Current form */
};
struct _form_window_class
{
  GnomeAppClass  parent_class;               /* The GnomeApp base class */
};
/****************************************************************************
 * Public widget manipulation functions
 ***************************************************************************/
GtkType          form_window_get_type              (void);
GtkWidget        *form_window_new                  (const gchar *geometry);
void             form_window_add_form              (form_window *window,
						    forms_def   *form);
void             form_window_del_form              (form_window *window,
						    forms_def   *form);
forms_def        *form_window_get_current_form     (form_window *window);

END_GNOME_DECLS
#endif
/*EOF*/

