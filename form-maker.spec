# Note that this is NOT a relocatable package
%define ver       0.0.3
%define rel       1
%define prefix    /usr

Summary: The form maker
Name: form-maker
Version: %ver
Release: %rel
Copyright: LGPL
Group: Applications/Publishing
Source: ftp://coco.comstar.net/pub/form-maker-%{ver}.tar.gz
BuildRoot: /tmp/form-maker-root
Obsoletes: form-maker
Packager: Gregory McLean <gregm@comstar.net>
Prereq: /sbin/install-info
Requires: gtk
Docdir: %{prefix}/doc

%description
This is a package specificly designed to aid office automation in that it will
take a specially prepared postscript template file and fill the template out
with data entered via a nice gui.

%changelog
* Thu Jul 22 1998 Gregory A. McLean <gregm@comstar.net>

-Initial spec file created.

%prep
%setup

%build

./configure --prefix=%prefix
if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{prefix} install

%clean
rm -rf $RPM_BUILD_ROOT

%post
%postun
%files
%defattr(-, root, root)

%doc AUTHORS COPYING ChangeLog NEWS README TODO
%attr(-, root, root) %{prefix}/bin/form_maker
