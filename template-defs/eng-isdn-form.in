# ISDN Engineering Worksheet.
#
form "ISDN Engineering Worksheet" {
   group "Engineering"
   path "@templatedir@/Engineering-ISDN-Template.ps"
   field "Customer Name" {
     macro "x-customer-name",
     tooltip "The customer's name.",
     size_max 80,
   }
   field "Customer Contact" {
     macro "x-customer-contact",
     tooltip "The contact person for this customer.",
     size_max 80,
   }
   field "Contact phone" {
     macro "x-customer-phone",
     tooltip "The phone number for the contact person.",
     size_max 80,
   }
   field "Contact fax" {
     macro "x-customer-fax",
     tooltip "The fax number for the contact person.",
     size_max 80,
   }
   field "Customer circuit ID" {
     macro "x-cust-circ-id",
     tooltip "The circuit id of the primary circuit for this customer.",
     size_max 80,
   }
   field "SPID 1" {
     macro "x-spid-1",
     tooltip "The first spid of the primary circuit for this customer.",
     size_max 20,
   }
   field "SPID 2" {
     macro "x-spid-2",
     tooltip "The second spid of the primary circuit for this customer.",
     size_max 20,
   }
   field "Link IP" {
     macro "x-cust-router-ip",
     tooltip "The ip address that the customer should set their router to.",
     size_max 25,
   }
   field "Number of channels" {
     macro "x-num-channels",
     tooltip "The number of channels this customer requested.",
     size_max 10,
   }
   field "Dialback" {
     macro "x-dialback",
     tooltip "Is this a dialback customer.",
     size_max 6,
   }
   field "Compression" {
     macro "x-compression",
     tooltip "The compression method requested for this circuiti.",
     size_max 20,
   }
   field "Destination router" {
     macro "x-comstar-router",
     tooltip "The name of the router that this customer connects to.",
     size_max 20,
   }
   field "Destination router IP" {
     macro "x-comstar-ip",
     tooltip "The ip address of the router this customer is connected to.",
     size_max 20,
     default "207.15.208.16",
   }
   field "Dialin number" {
     macro "x-dialin-number",
     tooltip "The number the customer dials to initiate this connection.",
     size_max 20,
   }
   field "Authentication Type" {
     macro "x-auth-type",
     tooltip "The authentication type for this circuit.",
     size_max 10,
     default "Chap",
   }
   field "Customer username" {
     macro "x-cust-user",
     tooltip "The username the customer's router should use when initiating the connection.",
     size_max 20,
   }
   field "Customer password" {
     macro "x-cust-pass",
     tooltip "The password the customer's router should use when initiating the connection.",
     size_max 20,
   }
   field "Allocated IP" {
     macro "x-allocated-ip",
     tooltip "The ip block that was allocated to this customer.",
     size_max 20,
   }
   field "Existing IP" {
     macro "x-existing-ip", 
     tooltip "Any existing ip block the customer needs routed on this circuit.",
     size_max 20,
   }
   field "Routing Protocol" {
     macro "x-routing-proto",
   }
   field "Primary DNS server" {
     macro "x-primary-dns",
     tooltip "The Primary dns server.",
     size_max 20,
     default "207.15.208.2",
   }
   field "Secondary DNS server" {
     macro "x-secondary-dns",
     tooltip "The secondary dns server.",
     size_max 20,
     default "207.15.208.3",
   }
   field "Customer domain name" {
      macro "x-cust-domain",
      tooltip "The primary domain name the customer would like for this circuit.",
      size_max 20,
   }
   field "Domain contact" {
      tooltip "The administrative contact for the domain, if any,",
      macro "x-cust-domain-contact",
      size_max 80,
   }
}

