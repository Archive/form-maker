#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="Form-Maker"

(test -f $srcdir/configure.in \
  && test -d $srcdir/app \
  && test -f $srcdir/app/Makefile.am) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level form-maker directory"
    exit 1
}

. $srcdir/macros/autogen.sh
