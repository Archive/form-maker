/* Form maker version and package information */

#undef FORMMAKER_MAJOR_VERSION
#undef FORMMAKER_MINOR_VERSION
#undef FORMMAKER_MICRO_VERSION
#undef FORMMAKER_VERSION
#undef PACKAGE

/* Some Defines */

#undef HAVE_SYS_TIME_H
#undef USE_GNOME
#undef USE_GLE
#undef templatedir
#undef HAVE_LIBSM
#undef ENABLE_NLS
#undef HAVE_CATGETS
#undef HAVE_GETTEXT
#undef HAVE_LC_MESSAGES
#undef HAVE_STPCPY

